---
layout: article
title: Dinheiro Público? Código Público!
created: 1505338768
date: 2017-09-13
featured: true
aliases:
- "/article/521/"
- "/node/521/"
- "/publiccode"
summary: |
  ANSOL desafia Governo e Parlamento Portugueses a criarem legislação que
  determine que o software criado para o sector público, financiado com dinheiro
  público, seja disponibilizado como software livre.

  Os serviços digitais usados e disponibilizados pela Administração Pública são a
  infra-estrutura crítica das nações democráticas do séc. XXI. Para criar
  sistemas de confiança, é necessário que as instituições públicas tenham
  controlo total sobre o software e os sistemas de computador, que constituem a
  nossa infra-estrutura digital.
---
<img src="https://ansol.org/attachments/pmpc-quote-snowden.en_.png">

**ANSOL desafia Governo e Parlamento Portugueses a criarem legislação que
determine que o software criado para o sector público, financiado com dinheiro
público, seja disponibilizado como software livre.**

Os serviços digitais usados e disponibilizados pela Administração Pública são a
infra-estrutura crítica das nações democráticas do séc. XXI. Para criar
sistemas de confiança, é necessário que as instituições públicas tenham
controlo total sobre o software e os sistemas de computador, que constituem a
nossa infra-estrutura digital. No entanto, é raro a Administração Pública ter
controlo sobre os sistemas que usa e disponibiliza, devido às licenças de
software restritivas que:

* Proíbem a partilha e adaptação de código, impedindo a cooperação entre
  instituições públicas e obrigando, sem necessidade, a criar sistemas de raíz
  para cada instituição;
* Estimulam a criação de monopólios, ao impedir a competitividade. Como
  resultado, a administração pública fica totalmente dependente de meia dúzia
  de empresas;
* São uma ameaça à segurança da nossa infra-estrutura digital por proibirem o
  acesso ao código-fonte, tornando extremamente difícil, se não mesmo
  impossível, corrigir buracos de segurança.

O software usado e disponibilizado pela Administração Pública é pago com
dinheiro público, de impostos de todos os cidadãos. É preciso que tal software
seja Software Livre e de Código Aberto porque:

* Só o Software Livre e de Código Aberto permite a qualquer entidade, em
  liberdade, usar, estudar, partilhar e melhorar as aplicações que usamos no
  dia-a-dia, permitindo que diferentes instituições públicas possam usar o
  mesmo software para as mesmas necessidades, bem como adaptar esse software
  para novas funcionalidades;
* Só as licenças de Software Livre e de Código Aberto têm salvaguardas contra a
  dependência de serviços de empresas ou fornecedores específicos;
* Só o Software Livre e de Código Aberto assegura o acesso ao código-fonte, que
  permite a correcção de buracos de segurança.

A Associação Nacional para o Software Livre (ANSOL) junta-se à [Free Software
Foundation Europe](https://fsfe.org/) e a dezenas de outras entidades,
incluindo a [Associação Ensino Livre](http://ensinolivre.pt) (AEL) e a
[Associação D3 – Defesa dos Direitos Digitais](https://direitosdigitais.pt), no
apelo aos governos e parlamentos europeus que façam aprovar e implementem
legislação que obrigue a que todo o software desenvolvido para o sector público
seja disponibilizado publicamente sob uma licença de Software Livre e de Código
Aberto.

**Se o dinheiro que paga o software é público, o código desse software tem de
ser público.**

**A ANSOL convida ainda todos os cidadãos a [assinarem a carta
aberta](https://publiccode.eu/openletter/), que será enviada aos nossos
representantes políticos.**

<iframe src="https://player.vimeo.com/video/279811204"
        width="640" height="360"
        frameborder="0"></iframe>

[Public Money? Public Code!](https://vimeo.com/279811204)
from [Free Software Foundation Europe](https://vimeo.com/fsfe)
on [Vimeo](https://vimeo.com).
