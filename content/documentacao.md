---
title: Documentação e Transparência
---

- [Estatutos]({{< ref "estatutos" >}})
- [Regulamento Interno]({{< ref "regulamento-interno" >}})
- [Reuniões da Direcção](https://membros.ansol.org/node/22)
- [Assembleias Gerais](https://membros.ansol.org/AG), apenas visíveis a membros da ANSOL


## Protocolos

A ANSOL assinou, em agosto de 2022, protocolos de parceria com os Agrupamentos de Escolas
de Santa Maria da Feira e Augusto Cabrita:

- [Protocolo de parceria com Agrupamento de Escolas de Santa Maria da Feira][protocolo-feira]
- [Protocolo de parceria com Agrupamento de Escolas Augusto Cabrita][protocolo-barreiro]

[protocolo-feira]: /protocolos/smfeira.pdf
[protocolo-barreiro]: /protocolos/barreiro.pdf
