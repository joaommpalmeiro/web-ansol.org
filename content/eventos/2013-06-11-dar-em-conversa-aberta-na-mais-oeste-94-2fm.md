---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 179
  event:
    location: 'Mais Oeste - 94.2FM '
    site:
      title: ''
      url: https://www.facebook.com/events/362254213896699/?notif_t=plan_user_invited
    date:
      start: 2013-06-11 21:00:00.000000000 +01:00
      finish: 2013-06-11 22:00:00.000000000 +01:00
    map: {}
layout: evento
title: '"DAR em Conversa Aberta" na Mais Oeste - 94.2FM'
created: 1370971399
date: 2013-06-11
aliases:
- "/evento/179/"
- "/node/179/"
---
<p>Neste programa falar-se-à sobre Licenças Open e a "Propriedade Intelectual Livre".</p>
