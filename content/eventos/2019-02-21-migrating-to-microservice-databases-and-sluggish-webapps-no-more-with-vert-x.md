---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 649
  event:
    location: Sala VA2, Pavilhão Civil, Instituto Superior Técnico, Lisboa
    site:
      title: ''
      url: https://www.meetup.com/pt-jug/events/259159517/
    date:
      start: 2019-02-27 18:30:00.000000000 +00:00
      finish: 2019-02-27 18:30:00.000000000 +00:00
    map: {}
layout: evento
title: Migrating to Microservice Databases & Sluggish webapps no more with Vert.x
created: 1550768451
date: 2019-02-21
aliases:
- "/evento/649/"
- "/node/649/"
---

