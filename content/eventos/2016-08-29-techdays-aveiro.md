---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 443
  event:
    location: Parque de Exposições de Aveiro
    site:
      title: ''
      url: http://www.techdays.pt/
    date:
      start: 2016-09-15 00:00:00.000000000 +01:00
      finish: 2016-09-17 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Techdays Aveiro
created: 1472500123
date: 2016-08-29
aliases:
- "/evento/443/"
- "/node/443/"
---
<p>O Techdays Aveiro é um evento de três dias, onde se falará um pouco de tudo sobre tecnologia.</p><p>Encontra a ANSOL no espaço expositivo durante o evento - estaremos junto dos nossos amigos da <a href="http://hackaveiro.org/">Hack'Aveiro</a>!</p>
