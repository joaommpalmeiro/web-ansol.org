---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 475
  event:
    location: 
    site:
      title: ''
      url: http://loco.ubuntu.com/events/ubuntu-pt/3484-ubuntu-hour-lisbon/
    date:
      start: 2017-01-03 18:30:00.000000000 +00:00
      finish: 2017-01-03 18:30:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Ubuntu-PT
created: 1482097781
date: 2016-12-18
aliases:
- "/evento/475/"
- "/node/475/"
---
<p>A comunidade Portuguesa de Ubuntu volta a encontrar-se, desta vez na "Burgers and Beers" do Centro Comercial Monumental, no Saldanha em Lisboa.</p>
