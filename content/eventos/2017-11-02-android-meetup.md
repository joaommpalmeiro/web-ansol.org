---
categories:
- android
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 78
  node_id: 525
  event:
    location: SPMS - Serviços Partilhados do Ministério da Saúde
    site:
      title: ''
      url: https://www.meetup.com/Android-LX/events/244754524
    date:
      start: 2017-11-23 19:00:00.000000000 +00:00
      finish: 2017-11-23 19:00:00.000000000 +00:00
    map: {}
layout: evento
title: Android Meetup
created: 1509657603
date: 2017-11-02
aliases:
- "/evento/525/"
- "/node/525/"
---

