---
categories:
- devops
- porto
- conferência
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 302
  - tags_tid: 142
  - tags_tid: 293
  node_id: 612
  event:
    location: FARO Technologies, Rotunda Engenheiro Edgar Cardoso, 23, 9º andar ·
      Vila Nova de Gaia, Portugal
    site:
      title: 'DevOps Porto #15'
      url: https://www.meetup.com/devopsporto/events/249229958/
    date:
      start: 2018-04-18 18:45:00.000000000 +01:00
      finish: 2018-04-18 20:45:00.000000000 +01:00
    map: {}
layout: evento
title: 'DevOps Porto #15: Acceptance Testing for Continuous Delivery'
created: 1523886487
date: 2018-04-16
aliases:
- "/evento/612/"
- "/node/612/"
---
<div class="chunk event-description--wrapper"><div class="event-description runningText"><p>Welcome to our 15th meetup about DevOps and it's culture, values and practices. This time around we will hosting Dave Farley (@davefarley77), the well known thought leader and co-author of the seminal book "Continuous Delivery". Currently, he is an independent software developer and consultant, and founder and director of Continuous Delivery Ltd. He will be talking about Acceptance Testing for Continuous Delivery.<br><br>Writing and maintaining a suite of acceptance tests that can give you a high level of confidence in the behaviour and configuration of your system is a complex task. In this talk Dave will describe approaches to acceptance testing that allow teams to: work quickly and effectively; build excellent functional coverage for complex enterprise-scale systems; manage and maintain those tests in the face of change, and of evolution in both the codebase and the understanding of the business problem.<br><br>This talk will answer the following questions, and more: How do you fail fast? How do you make your testing scalable? How do you isolate test cases from one-another? How do you maintain a working body of tests when you radically change the interface to your system?<br><br>Agenda:<br>• Welcome<br>• Acceptance Testing for Continuous Delivery by Dave Farley<br>• Open space forum<br>• Coffee &amp; Networking<br>• Closing<br><br>A special thanks to the DevOps Lisbon team for helping coordinate Dave's presence with us. He'll be presenting at their meetup on the 16th (<a href="https://www.meetup.com/pt-BR/DevOps-Lisbon/events/246008861/" target="__blank" title="https://www.meetup.com/pt-BR/DevOps-Lisbon/events/246008861/" class="link">https://www.meetup.com/pt-BR/DevOps-Lisbon/events/246008861/</a>)<br><br>Also, a special thanks to FARO Technologies (<a href="http://www.faro.com" target="__blank" title="www.faro.com" class="link">www.faro.com</a>) for sponsoring Dave's stay in Porto.</p></div></div><div class="attendees-sample"><div class="flex flex--row"><div class="flex-item"><h3 class="attendees-sample-total text--sectionTitle text--bold padding--bottom"><span>&nbsp;</span></h3></div></div></div>
