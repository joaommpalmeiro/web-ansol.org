---
layout: evento
title: MiniDebConf Lisbon
metadata:
  event:
    location: Técnico Lisboa, Campus Alameda, Av. Rovisco Pais, 1049-001 Lisboa
    site:
      url: https://pt2023.mini.debconf.org/
    date:
      start: 2023-02-12
      finish: 2023-02-16
---
