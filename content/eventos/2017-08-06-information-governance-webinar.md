---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 518
  event:
    location: 
    site:
      title: 
      url: 
    date:
      start: 2017-10-06 14:00:00.000000000 +01:00
      finish: 2017-10-06 16:00:00.000000000 +01:00
    map: {}
layout: evento
title: Information governance webinar
created: 1502054528
date: 2017-08-06
aliases:
- "/evento/518/"
- "/node/518/"
---
<p>In the context of ISA2's SEMIC Action ('Promoting semantic interoperability amongst the European Union Member States'), a study was conducted on 'Information governance and management for public administrations' aiming mainly to raise awareness among the Member States of the importance and the benefits of information governance, and provide an overview of high level good practices based on several relevant case studies.</p><p>Throughout the study, various aspects related to information governance and management were looked, such as: principles, roles and responsibilities, reference, metadata and master data management, information quality, etc.</p><p>During the work, it was learned that while a lot happens in the field of information governance and management, organisations can do more to breakdown their silos and get the most out of their data, information and knowledge.</p><p>Join an open conversation, where together, we'll try to answer questions, such as: 'who within an organisation is best positioned to receive the mandate and implement an information governance and management strategy?', 'how should an organisation define the scope of their information governance and management strategy?', 'how to deal with data and information quality?', 'how to raise awareness, engage and commit staff to improve the way they manage data, information and knowledge?', etc.</p><p>Bring your own questions, concerns, or thoughts which you would like to share during the webinar. </p><p><strong>Save the date: 6 Friday, October 2017, between 14:00 - 16:00</strong></p><p>More practical information, together with a link to the published study, will come soon after the summer break, by the ISA2 team.</p>
