---
categories: []
metadata:
  node_id: 17
  event:
    location: ISEP, Porto
    site:
      title: http://porto2012.drupal-pt.org/
      url: http://porto2012.drupal-pt.org/
    date:
      start: 2012-05-04 00:00:00.000000000 +01:00
      finish: 2012-05-05 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: DrupalCamp Porto 2012
created: 1333223909
date: 2012-03-31
aliases:
- "/evento/17/"
- "/node/17/"
---
<p>Dois dias totalmente focados no Drupal e tecnologias anexas que re&uacute;ne programadores, webdesigners que usam, ponderam usar ou sentem curiosidade por conhecer o Drupal.</p>
