---
categories:
- ttip
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 280
  event:
    location: Streaming
    site:
      title: EbS
      url: http://ec.europa.eu/avservices/ebs/schedule.cfm?page=1&date=02/06/2015&institution=0#s284658
    date:
      start: 2015-02-06 14:30:00.000000000 +00:00
      finish: 2015-02-06 14:30:00.000000000 +00:00
    map: {}
layout: evento
title: 'TTIP: conferência de imprensa'
created: 1423147192
date: 2015-02-05
aliases:
- "/evento/280/"
- "/node/280/"
---
<p>De 2 a 6 de fevereiro, realiza-se em Bruxelas a oitava ronda de negociações da Parceria Transatlântica de Comércio e Investimento (TTIP) entre a UE e os EUA.</p><p>O negociador principal da UE, Ignacio Garcia Bercero, e o negociador principal dos EUA, Dan Mullaney, organizarão a conferência de imprensa de encerramento na sexta-feira, 6 de fevereiro, às 14h30 (hora de Lisboa).</p><p>A conferência de imprensa pode ser seguida em direto na EbS.</p>
