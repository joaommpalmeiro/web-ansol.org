---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 690
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/264084806/
    date:
      start: 2019-09-12 00:00:00.000000000 +01:00
      finish: 2019-09-12 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt @ Sintra
created: 1566150705
date: 2019-08-18
aliases:
- "/evento/690/"
- "/node/690/"
---

