---
categories:
- '2013'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 32
  node_id: 212
  event:
    location: " Xarevision  Rua Ciríaco Cardoso, 265 - i, Porto"
    site:
      title: Página oficial do encontro
      url: http://portolinux.org/doku.php?id=encontrostecnicos:nov13
    date:
      start: 2013-11-09 15:00:00.000000000 +00:00
      finish: 2013-11-09 20:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro técnico de Novembro 2013
created: 1383779237
date: 2013-11-06
aliases:
- "/evento/212/"
- "/node/212/"
---
<div class="level1"><p>Vai-se realizar no dia <strong>9 de Novembro de 2013</strong> pelas <strong>15h</strong> um encontro técnico com o seguinte alinhamento:</p><ul><li class="level2"><div class="li"><a href="http://fluentd.org/"><strong>FluentD</strong></a> - análise de logs , primeiro contacto, 1a parte por César Araújo e 2a parte por Tomás Lima</div></li><li class="level2"><div class="li"><strong>Magusto</strong> (improvisado)</div></li><li class="level2"><div class="li"><a href="https://www.docker.io/"><strong>Docker</strong></a> – Containers para seres urbanos, por José Pinheiro Neta</div></li></ul></div>
