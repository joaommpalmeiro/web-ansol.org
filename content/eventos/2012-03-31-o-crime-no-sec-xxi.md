---
categories: []
metadata:
  node_id: 15
  event:
    location: Sede do IINFACTS/Museu de Penafiel
    site:
      title: http://ciencias.iscsn.cespu.pt/eventos/2012/o-crime-no-sec-xxi.html
      url: http://ciencias.iscsn.cespu.pt/eventos/2012/o-crime-no-sec-xxi.html
    date:
      start: 2012-04-20 08:00:00.000000000 +01:00
      finish: 2012-04-21 20:00:00.000000000 +01:00
    map: {}
layout: evento
title: O Crime no séc. XXI
created: 1333179536
date: 2012-03-31
aliases:
- "/evento/15/"
- "/node/15/"
---
<p>As VI Jornadas de Ci&ecirc;ncias do ISCS-N, entituladas &quot;O Crime no s&eacute;c. XXI&quot;, s&atilde;o compostas por tr&ecirc;s sess&otilde;es, a terceira intitulada &quot;O crime espreita novos paradigmas&quot;. Nesse &acirc;mbito a ANSOL ir&aacute; fazer uma apresenta&ccedil;&atilde;o sobre o tema &quot;Censura na Web?&quot;.</p>
