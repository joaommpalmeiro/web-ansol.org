---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 480
  event:
    location: Lisboa
    site:
      title: ''
      url: https://joinup.ec.europa.eu/node/158329/
    date:
      start: 2017-03-29 00:00:00.000000000 +01:00
      finish: 2017-03-29 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Sharing & Reuse Conference 2017
created: 1485104053
date: 2017-01-22
aliases:
- "/evento/480/"
- "/node/480/"
---

