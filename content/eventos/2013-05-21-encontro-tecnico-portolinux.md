---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 163
  event:
    location: Porto
    site:
      title: ''
      url: http://s.clix.pt/bl
    date:
      start: 2013-05-25 14:00:00.000000000 +01:00
      finish: 2013-05-25 21:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Técnico PortoLinux
created: 1369129164
date: 2013-05-21
aliases:
- "/evento/163/"
- "/node/163/"
---
<ul><li><span>Dia a Dia de um Devops na Blip - Miguel Fonseca</span></li><li><span><span>"Bring it to the cloud" -&nbsp;</span><span>Hugo Tavares, CTO da Lunacloud</span></span></li><li><span><span>Pequena apresentação da Cultura da Blip e uma pequena visita às instalações</span></span></li></ul>
