---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 530
  event:
    location: 
    site:
      title: ''
      url: http://osgeopt.pt/sasig2017/
    date:
      start: 2017-11-20 00:00:00.000000000 +00:00
      finish: 2017-11-22 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 7º Encontro Nacional de Software Aberto SIG - SASIG 2017
created: 1511111496
date: 2017-11-19
aliases:
- "/evento/530/"
- "/node/530/"
---

