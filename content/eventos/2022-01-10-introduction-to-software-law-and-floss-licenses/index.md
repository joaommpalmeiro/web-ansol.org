---
layout: evento
title: Introduction to Software Law & FLOSS Licences
metadata:
  event:
    location: Online
    site:
      url: https://www.projekte.hu-berlin.de/de/gnuHU/projekte/floss-werkstatt/veranstaltungen/wise-21-22/introduction-to-software-law-and-floss-licences-10-01-22
    date:
      start: 2022-01-10 17:00:00.000000000 +00:00
      finish: 2022-01-10 18:00:00.000000000 +00:00
---

What are FLOSS licences and what does "Copyleft" mean? What is the difference
between Free Software and Open Source Software? Guest Lecture by Dr. Lucas
Lasota

In his guest lecture on Monday, 10.01.22, starting at 6 pm, Dr. Lucas Lasota
will give us a short introduction to the law of free software licences. Are you
wondering what "copyleft" actually means and how free software or FLOSS and
open source software differ from each other legally? Or do you simply want to
learn more about free software in general? In any case, we look forward to your
participation in the lecture and our subsequent discussion. The talk and
discussion will be held in English, online via BigBlueButton. Prior knowledge
is not required.

Dr. Lucas Lasota is Legal Project Manager at the Free Software Foundation
Europe and an international counsel with a background in contract and
technology law. In addition to his commitment to free software and digital
autonomy, he is also a research assistant at the chair of Prof. Dr. Herbert
Zech at the law faculty of HU Berlin.
