---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAAkGfwhwLdRL10GU0NA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38648631713967e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.8992379307747e1
    mapa_left: !ruby/object:BigDecimal 27:-0.8992379307747e1
    mapa_top: !ruby/object:BigDecimal 27:0.38648631713967e2
    mapa_right: !ruby/object:BigDecimal 27:-0.8992379307747e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38648631713967e2
    mapa_geohash: eyceg7gbw75uruxz
  slide:
  - slide_value: 0
  node_id: 631
  event:
    location: Moita
    site:
      title: ''
      url: https://moita2018.softwarelivre.eu/
    date:
      start: 2018-11-23 15:00:00.000000000 +00:00
      finish: 2018-11-25 19:00:00.000000000 +00:00
    map: {}
layout: evento
title: IIª Festa do Software Livre - Moita 2018
created: 1541358974
date: 2018-11-04
aliases:
- "/evento/631/"
- "/moita2018/"
- "/node/631/"
---
<img src="https://ansol.org/attachments/moita.png" alt="Festa do Software Livre na Moita - cartaz">

A primeira Festa do Software Livre da Moita decorreu nas instalações da extinta
Universidade Independente, no Palheirão, em 2004, co-organizada pela Humaneasy
Consulting e pela ANSOL – Associação Nacional para o Software Livre, e contou
com o apoio do Município da Moita e de várias entidades e comunidades ligadas
ao Software Livre.

Esta segunda edição da Festa do Software Livre é um espaço para instalação
gratuita de software nos computadores dos visitantes e incluirá um programa de
palestras, demonstrações, workshops, informação, hackatons, animação com música
ao vivo e outras atracções. Durante o evento, a Comunidade Ubuntu Portugal
organiza a primeira UbuCon de Portugal e temos a presença da Wikimedia, Drupal
Portugal, D3, RepRap, Linux Tech, entre outros.

TRAZ UM AMIGO E O COMPUTADOR TAMBÉM!

Contactos: <a href="mailto:moita2018@softwarelivre.eu">moita2018@softwarelivre.eu</a>
