---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 312
  event:
    location: Avenida da Liberdade, Lisboa
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/manifestacao-dia-25-de-abril-lisboa/
    date:
      start: 2015-04-25 00:00:00.000000000 +01:00
      finish: 2015-04-25 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Dia da Liberdade
created: 1429828220
date: 2015-04-23
aliases:
- "/evento/312/"
- "/node/312/"
---
<p>Todos os anos a ANSOL vai à rua no 25 de Abril - Dia da Liberdade - para dar a conhecer a liberdade que vem do Software Livre.</p><p>Este ano, juntamo-nos à manifestação contra o TTIP e o CETA.</p>
