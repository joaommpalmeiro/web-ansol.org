---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 191
  event:
    location: 
    site:
      title: 
      url: 
    date:
      start: 2003-10-17 00:00:00.000000000 +01:00
      finish: 2003-10-17 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 4ª Conferência da Associação Portuguesa de Sistemas de Informação
created: 1371949746
date: 2013-06-23
aliases:
- "/evento/191/"
- "/node/191/"
---
<div id="content" lang="en" dir="ltr"><p class="line862">Sessão plenária sobre Software Livre. Inserida na 4ª Conferência da Associação Portuguesa de Sistemas de Informação. Universidade Portucalense, Porto. Jaime Villate (ANSOL)&nbsp;faz parte dos oradores convidados.<span id="line-4" class="anchor"></span><span id="bottom" class="anchor"></span></p><div>&nbsp;</div></div>
