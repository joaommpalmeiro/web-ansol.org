---
categories:
- ttip
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 251
  event:
    location: Lisboa
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/jantar-de-apoio-debate-da-plataforma-nao-a-parceria-transatlantica-ttip-612/
    date:
      start: 2014-12-06 20:00:00.000000000 +00:00
      finish: 2014-12-06 20:00:00.000000000 +00:00
    map: {}
layout: evento
title: Jantar de Apoio + Debate da Plataforma Não à Parceria Transatlântica (TTIP)
created: 1417608089
date: 2014-12-03
aliases:
- "/evento/251/"
- "/node/251/"
---
<p><img src="https://www.nao-ao-ttip.pt/wp-content/uploads/2014/12/jantar-debate-1024x378.jpg" width="1024" height="378"></p>
