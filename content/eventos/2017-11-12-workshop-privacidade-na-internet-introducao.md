---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 529
  event:
    location: Biblioteca Orlando Ribeiro, Telheiras, Lisboa
    site:
      title: ''
      url: https://www.meetup.com/Internet-Freedom-Lisbon/events/244464201/
    date:
      start: 2017-11-18 16:00:00.000000000 +00:00
      finish: 2017-11-18 16:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Workshop: Privacidade na Internet (Introdução)'
created: 1510511058
date: 2017-11-12
aliases:
- "/evento/529/"
- "/node/529/"
---

