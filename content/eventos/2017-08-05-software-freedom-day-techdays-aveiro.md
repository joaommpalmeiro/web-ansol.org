---
categories:
- sfd
- dia do software livre
- software freedom day
- drupal
- drupal-pt
- "#publiccode"
- debate
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 99
  - tags_tid: 197
  - tags_tid: 47
  - tags_tid: 64
  - tags_tid: 198
  - tags_tid: 196
  - tags_tid: 48
  node_id: 515
  event:
    location: Aveiro Expo – Parque de Exposições de Aveiro
    site:
      title: ''
      url: http://techdays.pt/#program
    date:
      start: 2017-10-14 00:00:00.000000000 +01:00
      finish: 2017-10-14 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Software Freedom Day - TechDays Aveiro
created: 1501967122
date: 2017-08-05
aliases:
- "/SFD2017/"
- "/evento/515/"
- "/node/515/"
---
Em 2017, a ANSOL celebrou o Software Freedom Day no TechDays em Aveiro, juntamente com a comunidade Drupal PT.</p><p>&nbsp;<img src="https://groups.drupal.org/files/drupal_day_2017.jpg" width="640" height="360">

### Debate VIP: "Com dinheiro público, só Software Livre" - 10h - 12h30

Imagine que o Estado vai comprar um carro de uma marca muita boa e muito
avançada. No entanto, em nenhuma circunstância pode abrir o capô do carro.
Todos os consertos ou alterações só podem ser feitos na empresa que o vendeu e
a sua condução obriga a uma formação especializada que só existe nessa mesma
empresa. Assim o Estado, com o dinheiro dos contribuintes, vê-se obrigado a
pagar não só pelo carro mas também por contratos de manutenção que lhe ficam
caros. Pode chamar a este carro de "software" e o Estado gasta cerca de 180
milhões de euros anualmente nestas condições. Será isto um bom negócio para o
Estado? Haverá alternativas?

A ANSOL - Associação Nacional para o Software livre vai promover um debate VIP
intitulado "Com dinheiro público, só Software Livre". O evento ocorre no dia 14
de Outubro de 2017 entre as 10h e as 12h30m em Aveiro e está integrado no
[Techdays](https://www.techdays.pt), o grande fórum de tecnologia em Portugal.

Ao estilo do debate universitário, de modo divertido e informativo, com a ajuda
da Sociedade de Debates da Universidade do Porto, convidamos a Engª Ana I.
Branco (Agência para Modernização Administrativa), Prof. Carlos Costa (Lisbon
School of Economics & Management) e Engº Luís Sousa (ASSOFT - Associação
Portuguesa de Software) para esgrimir argumentos e responder às questões do
público.

Não perca este grande debate! Registe-se em
[www.techdays.pt](https://www.techdays.pt) e inscreva-se na sessão
"CONFERÊNCIA: Software Livre e Tecnologia Drupal" para reservar o seu lugar no
auditório!

Gravação audio (parcial) em https://ansol.org/recordings/techdays1.ogg .

### Drupal Day - 15h

À tarde, no Auditório Principal, e em conjunto com a comunidade Portuguesa de
Drupal, haverá uma série de apresentações relacionadas com Software Livre no
geral e Drupal em particular:

* 15:00 - Software Livre em Portugal - Marcos Marado (ANSOL)
* 15:35 - Cultural Heritage &amp; Drupal - Alberto Permury
* 16:10 - Drupal 8: Internet of Things - Rui Figueiredo
* 16:45 - Encerramento
* 17:00 - Coffee Break e Networking

Gravação audio:

<iframe src="https://archive.org/embed/DupalDayPT2017"
        width="500" height="140"
        frameborder="0"></iframe>
