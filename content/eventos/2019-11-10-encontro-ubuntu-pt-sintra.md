---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 708
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/266300166
    date:
      start: 2019-11-14 00:00:00.000000000 +00:00
      finish: 2019-11-14 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Ubuntu-pt @ Sintra
created: 1573416033
date: 2019-11-10
aliases:
- "/evento/708/"
- "/node/708/"
---

