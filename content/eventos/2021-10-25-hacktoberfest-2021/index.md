---
metadata:
  event:
    location: Online
    site:
      url: "https://matrix.to/#/#Hacktoberfest2021:ansol.org"
    date:
      start: 2021-10-23 10:00:00.000000000 +01:00
      finish: 2021-10-31 23:59:59.000000000 +01:00
title: Hacktoberfest 2021 - Hackatão ANSOL
layout: evento
showcover: false
aliases:
- /hacktoberfest2021/
---

![ANSOL x Hacktoberfest 2021. Abertura: 23 de Outubro, às 10h00. Fecho: 31 de Outubro, às 23h59. Local: https://matrix.to/#/#Hacktoberfest2021:ansol.org](poster.png)
