---
categories:
- hackathon
- tecnologia
- concurso de proghramação
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAABAABA+SAOwJ6KKKGGKkRA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.40332233567043e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.3766100406647e1
    mapa_left: !ruby/object:BigDecimal 27:-0.3766100406647e1
    mapa_top: !ruby/object:BigDecimal 27:0.40332233567043e2
    mapa_right: !ruby/object:BigDecimal 27:-0.3766100406647e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.40332233567043e2
    mapa_geohash: ezjm6q3873rbxuxv
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 119
  - tags_tid: 108
  - tags_tid: 315
  node_id: 656
  event:
    location: University Carlos III of Madrid Auditorium
    site:
      title: T3chfest
      url: https://t3chfest.uc3m.es/2019/
    date:
      start: 2019-03-14 00:00:00.000000000 +00:00
      finish: 2019-03-15 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: T3chfest
created: 1552070352
date: 2019-03-08
aliases:
- "/evento/656/"
- "/node/656/"
---
<p>T3chFest 2019 has a <strong> variety and complete</strong> agenda, filled with technical and inspiring talks. This is what you can find along five different tracks during Thursday 14th and Friday 15th March!</p><p>In this edition, <strong>workshops have been moved to a different date:</strong> they will take place on Saturday 9th March at La Nave Madrid. More info soon.</p><p><strong>Free registration is not open yet</strong>. Subscribe to <a href="https://t3chfest.uc3m.es/newsletter" target="_blank">our newsletter</a> or follow us in social media to keep yourself updated.</p><p>Para acceder a las charlas solo necesitas <strong><a href="https://t3chfest2019.eventbrite.es/?aff=web" target="_blank">conseguir tu entrada gratis</a></strong>.</p>
