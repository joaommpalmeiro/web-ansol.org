---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 803
  event:
    location: Online
    site:
      title: ''
      url: https://op.europa.eu/en/web/euopendatadays/
    date:
      start: 2021-11-23 00:00:00.000000000 +00:00
      finish: 2021-11-25 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: EU Open Data Days
created: 1622125222
date: 2021-05-27
aliases:
- "/evento/803/"
- "/node/803/"
---

As primeiras Jornadas de Dados Abertos da União Europeia decorrerão entre 23 e
25 de novembro de 2021. Servindo de pólo de difusão de conhecimento e trazendo
os benefícios dos dados abertos para o setor público da UE e, através dele,
para as pessoas e as empresas, trata-se de um evento que procurará servir de
inspiração e de montra de divulgação para as tendências mais recentes e as
soluções mais inovadoras desta área.
