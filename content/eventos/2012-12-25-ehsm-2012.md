---
categories: []
metadata:
  node_id: 115
  event:
    location: 
    site:
      title: ''
      url: http://ehsm.eu/index.html
    date:
      start: 2012-12-28 00:00:00.000000000 +00:00
      finish: 2012-12-30 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: EHSM 2012
created: 1356475634
date: 2012-12-25
aliases:
- "/evento/115/"
- "/node/115/"
---
<p>Exceptionally Hard &amp; Soft Meeting 2012 - um evento sobre software e hardware livres na comunidade DIY, em Berlin, com live streaming do evento, em Ingl&ecirc;s, dispon&iacute;vel gratuitamente.</p>
