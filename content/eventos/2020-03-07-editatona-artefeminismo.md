---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 738
  event:
    location: Porto
    site:
      title: ''
      url: https://pt.wikipedia.org/wiki/Wikip%C3%A9dia:Edit-a-thon/Atividades_em_portugu%C3%AAs/Artefeminismo/Porto_2020
    date:
      start: 2020-03-15 00:00:00.000000000 +00:00
      finish: 2020-03-15 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Editatona Arte+Feminismo
created: 1583615665
date: 2020-03-07
aliases:
- "/evento/738/"
- "/node/738/"
---

