---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 461
  event:
    location: 
    site:
      title: ''
      url: http://www.meetup.com/Android-LX/events/234529873/?rv=ea1&_af=event&_af_eid=234529873&https=off
    date:
      start: 2016-10-12 19:00:00.000000000 +01:00
      finish: 2016-10-12 21:00:00.000000000 +01:00
    map: {}
layout: evento
title: Introduction to RxJava for Android Developers
created: 1475329534
date: 2016-10-01
aliases:
- "/evento/461/"
- "/node/461/"
---

