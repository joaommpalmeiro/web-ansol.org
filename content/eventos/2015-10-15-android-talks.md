---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 350
  event:
    location: Coimbra
    site:
      title: 
      url: 
    date:
      start: 2015-10-21 00:00:00.000000000 +01:00
      finish: 2015-10-21 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Android Talks
created: 1444865209
date: 2015-10-15
aliases:
- "/evento/350/"
- "/node/350/"
---
<p style="font-size: 12.8px; font-family: arial,sans-serif;">A primeira edição das&nbsp;<strong>Android Talks</strong>, um evento sobre desenvolvimento, negócio e design de aplicações Android vai-se realizar no próximo dia<strong>&nbsp;21 de Outubro</strong>, das 19 às 20h30.</p><p style="font-size: 12.8px; font-family: arial,sans-serif;">&nbsp;<strong>Talks:</strong></p><p style="font-size: 12.8px; font-family: arial,sans-serif;">-&nbsp;Kotlin 101&nbsp;por Ivan Burel, Faber Ventures</p><p style="font-size: 12.8px; font-family: arial,sans-serif;">- Android TV Strangelove (Or How I Learned to Stop Worrying And Love The Bomb) por Carlos Mota, WIT Software</p><p style="font-size: 12.8px; font-family: arial,sans-serif;"><strong>Local:</strong></p><p style="font-size: 12.8px; font-family: arial,sans-serif;">Nest Collective (<a href="http://nestcollective.co/" target="_blank">http://nestcollective.co</a>), no 6º andar do Centro Comercial Avenida (Coimbra).</p><p style="font-size: 12.8px; font-family: arial,sans-serif;">Apareçam!</p><p style="font-size: 12.8px; font-family: arial,sans-serif;">G+:&nbsp;<a href="https://plus.google.com/events/ce50hmne34tbgm4tu160aoaiuck" target="_blank">https://plus.google.com/events/ce50hmne34tbgm4tu160aoaiuck</a></p><p style="font-size: 12.8px; font-family: arial,sans-serif;">Facebook:&nbsp;<a href="https://www.facebook.com/events/431220813749779/" target="_blank">https://www.facebook.com/events/431220813749779/</a></p>
