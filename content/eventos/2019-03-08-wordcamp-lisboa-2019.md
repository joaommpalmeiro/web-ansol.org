---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAD0jE4iwL9ZzOPEX0NA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38748196100979e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9153419137001e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9153419137001e1
    mapa_top: !ruby/object:BigDecimal 27:0.38748196100979e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9153419137001e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38748196100979e2
    mapa_geohash: eyckrtystg7cpgpz
  slide:
  - slide_value: 0
  node_id: 655
  event:
    location: ISCTE, Lisboa
    site:
      title: WordPress Portugal
      url: https://2019.lisboa.wordcamp.org
    date:
      start: 2019-05-18 00:00:00.000000000 +01:00
      finish: 2019-05-19 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: WordCamp Lisboa 2019
created: 1552045402
date: 2019-03-08
aliases:
- "/WordCampLisboa2019/"
- "/evento/655/"
- "/node/655/"
---
O WordCamp volta a Lisboa em 2019, a 18 e 19 de Maio, no ISCTE. 
Entretanto, subscrevam as actualizações, contactem a equipa e sigam também as novidades no WP-Portugal.com .
Façam parte do mais importante evento WordPress em Portugal.
