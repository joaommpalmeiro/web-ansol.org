---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 482
  event:
    location: Porto
    site:
      title: ''
      url: http://www.transparenciahackday.org/2017/02/open-data-day-esta-quase-anda-dai/
    date:
      start: 2017-03-04 00:00:00.000000000 +00:00
      finish: 2017-03-04 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Open Data Day
created: 1486585731
date: 2017-02-08
aliases:
- "/evento/482/"
- "/node/482/"
---
<p>O Open Data Day é uma celebração anual de dados abertos por todo o mundo. É uma oportunidade para mostrar os benefícios dos dados abertos e encorajar a adopção de políticas de dados abertos no governo, em empresas e na sociedade civil.</p><p>Em Portugal a data também será celebrada, no Porto.</p>
