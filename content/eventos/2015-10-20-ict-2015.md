---
categories:
- informática
- tecnologia
- agenda digital europeia
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 107
  - tags_tid: 108
  - tags_tid: 109
  node_id: 353
  event:
    location: Lisboa
    site:
      title: ICT 2015
      url: http://ec.europa.eu/digital-agenda/ICT2015
    date:
      start: 2015-10-20 00:00:00.000000000 +01:00
      finish: 2015-10-22 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: ICT 2015
created: 1445378747
date: 2015-10-20
aliases:
- "/evento/353/"
- "/node/353/"
---
<p>ICT 2015 - Innovate, Connect, Transform<br><br>20-22 October 2015 in Lisbon, Portugal, organised by the European Commission, together with the Fundação para a Ciência e a Tecnologia.<br><br>The ICT 2015 event will comprise a number of parallel activities:<br><br>A policy conference presenting the new Commission's policies and initiatives on Research &amp; Innovation in ICT (Horizon 2020 Programme);<br>An interactive exhibition showcasing the best results and impact of most recent EU ICT Research &amp; Innovation;<br>Many networking opportunities to enhance quality partnerships, help participants find partners, connect Research and Innovation and trigger collaboration;<br>Horizon 2020 Work Programme 2016-2017 thematic sessions, offering detailed information on the funding opportunities in ICT sector;<br>The Startup Europe Forum, offering a set of activities profiling EU policy actions for startups and SMEs, innovators, private and public investors.</p>
