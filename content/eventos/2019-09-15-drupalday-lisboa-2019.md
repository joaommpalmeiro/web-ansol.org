---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 695
  event:
    location: Lisboa
    site:
      title: ''
      url: https://www.eventbrite.com/e/drupalday-lisboa-2019-tickets-70024034773
    date:
      start: 2019-11-22 00:00:00.000000000 +00:00
      finish: 2019-11-22 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: DrupalDay Lisboa 2019
created: 1568503235
date: 2019-09-15
aliases:
- "/evento/695/"
- "/node/695/"
---

