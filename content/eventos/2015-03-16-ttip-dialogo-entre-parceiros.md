---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 294
  event:
    location: Lisboa
    site:
      title: ''
      url: http://ec.europa.eu/portugal/comissao/destaques/20150312_ttip_dialogo_parceiros_pt.htm
    date:
      start: 2015-03-26 00:00:00.000000000 +00:00
      finish: 2015-03-27 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'TTIP: diálogo entre parceiros'
created: 1426540610
date: 2015-03-16
aliases:
- "/evento/294/"
- "/node/294/"
---
<p>O evento pode também ser acompanhado online, em https://twitter.com/search?f=realtime&amp;q=%23dialogoTTIP&amp;src=tyah .</p>
