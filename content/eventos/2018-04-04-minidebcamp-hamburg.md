---
categories:
- debian
- camp
- minidebconf
- minidebcamp
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 66
  - tags_tid: 254
  - tags_tid: 255
  - tags_tid: 256
  node_id: 574
  event:
    location: Victoria Kaserne, Hamburgo, Alemanhas
    site:
      title: MiniDebCamp
      url: https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg
    date:
      start: 2018-05-16 00:00:00.000000000 +01:00
      finish: 2018-05-18 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: MiniDebCamp Hamburg
created: 1522856731
date: 2018-04-04
aliases:
- "/evento/574/"
- "/node/574/"
---
<p>Um MiniDebCamp é um período de tempo em que os developers e voluntários Debian se organizam para trabalhar em conjunto no projecto Debian Linux. Normalmente estes campos precedem um encontro Debconf.</p>
