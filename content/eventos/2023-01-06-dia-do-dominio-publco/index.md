---
layout: evento
title: Dia do Domínio Público 2023
metadata:
  event:
    date:
      start: 2023-01-06
      finish: 2023-01-06
    location: Lisboa
    site:
      url: https://pt.wikimedia.org/wiki/Dia_do_Dom%C3%ADnio_P%C3%BAblico_2023
---

