---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 627
  event:
    location: 
    site:
      title: ''
      url: http://datewithdata.pt/
    date:
      start: 2018-09-08 00:00:00.000000000 +01:00
      finish: 2018-09-08 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Date With Data #30'
created: 1536325102
date: 2018-09-07
aliases:
- "/evento/627/"
- "/node/627/"
---
<h3 class="m_5600766826392774019null" style="display: block; margin: 0; padding: 0; color: #202020; font-family: Helvetica; font-size: 20px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;">Que tal está o tempo lá fora?</h3><div class="m_5600766826392774019row m_5600766826392774019text-center"><div class="m_5600766826392774019row m_5600766826392774019text-center"><div class="m_5600766826392774019small-12 m_5600766826392774019medium-10 m_5600766826392774019medium-offset-1 m_5600766826392774019small-offset-0 m_5600766826392774019columns"><p style="color: #333333; font-family: Roboto,'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; text-align: left; margin: 10px 0; padding: 0; line-height: 150%;">No próximo encontro, vamos olhar para a meteorologia: temperaturas, ventos, pressões, previsões, registos históricos, marés e o que mais por aí houver.</p><p style="color: #333333; font-family: Roboto,'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; text-align: left; margin: 10px 0; padding: 0; line-height: 150%;">O clima tem feito manchetes nos últimos anos e não tem sido pelas melhores razões. Tanto as alterações climáticas como os incêndios florestais têm marcado a atualidade e nós queremos estar (ainda mais) em cima do assunto. Vamos dedicar-nos a encontrar fontes de informação, criar datasets e pensar juntos que coisas interessantes podemos projetar para percebermos melhor o que se vai passando com o tempo.</p><p style="color: #333333; font-family: Roboto,'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; text-align: left; margin: 10px 0; padding: 0; line-height: 150%;">Este Sábado, dia 8/Setembro, vai estar um dia de sol e temos uma fantástica sala num jardim com bastante sombra no centro do Porto!<br> Traz o portátil, uma tripla para assegurar que toda a gente tem bateria e boa disposição!</p><p style="color: #333333; font-family: Roboto,'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; text-align: left; margin: 10px 0; padding: 0; line-height: 150%;">Das 10:00 às 17:00, no&nbsp;<a href="https://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=9f3319aa1b&amp;e=b684bb14e9" target="_blank" style="color: #2baadf; font-weight: normal; text-decoration: underline;" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://transparenciahackday.us6.list-manage.com/track/click?u%3D7c39a9eb2d8160fd5975e6fec%26id%3D9f3319aa1b%26e%3Db684bb14e9&amp;source=gmail&amp;ust=1536404870305000&amp;usg=AFQjCNFxl8E2UTR4Qdr_cutlrMKtMdmF3w">Pavilhão Jardim do UPTEC PINC</a>&nbsp;(Praça Coronel Pacheco) – aparece e traz um amigo/a também!</p><p style="color: #333333; font-family: Roboto,'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; text-align: left; margin: 10px 0; padding: 0; line-height: 150%;">Aponta já na agenda: o próximo Date With Data será a 20 de Outubro!</p></div></div></div>
