---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 519
  event:
    location: São Mamede de Infesta
    site:
      title: 
      url: 
    date:
      start: 2017-08-15 12:00:00.000000000 +01:00
      finish: 2017-08-15 14:00:00.000000000 +01:00
    map: {}
layout: evento
title: Almoço-convívio para sócios da ANSOL
created: 1502479543
date: 2017-08-11
aliases:
- "/evento/519/"
- "/node/519/"
---
<p>Precedendo a <a href="https://ansol.org/node/516">Assembleia Geral</a>, a ANSOL está a promover um almoço de convívio para os seus associados.</p><p>Inscrições devem ser feitas via e-mail para a lista de sócios.</p><p>&nbsp;</p><p>Mais informações sobre este evento podem ser encontradas também nessa lista.</p>
