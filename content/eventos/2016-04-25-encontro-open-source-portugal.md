---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 416
  event:
    location: IAPMEI
    site:
      title: ''
      url: http://ospt.artica.cc/
    date:
      start: 2016-05-30 00:00:00.000000000 +01:00
      finish: 2016-05-30 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Open Source Portugal
created: 1461605402
date: 2016-04-25
aliases:
- "/evento/416/"
- "/node/416/"
---
<p><img src="http://artica.cc/assets/images/2016-04-21-ospt.jpg" alt="Poster" width="800" height="1131"></p>
