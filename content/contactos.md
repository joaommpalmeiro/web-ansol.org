---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 35
layout: page
title: Contactos
created: 1334481282
date: 2012-04-15
aliases:
- "/contacto/"
- "/contato/"
- "/node/35/"
- "/page/35/"
---

## Correio Electrónico

<dl>
  <dt><strong>Geral:</strong></dt>
  <dd><a href="mailto:contacto@ansol.org">contacto@ansol.org</a></dd>
  <dt><strong>Sobre o site:</strong></dt>
  <dd><a href="mailto:webmaster@ansol.org">webmaster@ansol.org</a></dd><dt><strong>Discussão e participação:</strong></dt><dd><a href="https://membros.ansol.org/mailman/listinfo">listas de correio</a></dd></dl>

Poderá também consultar e contactar os **[órgãos sociais em exercício](/orgaos-sociais)**.

## Redes sociais

Estamos presentes no [Twitter](https://twitter.com/ANSOL) e no [Mastodon](https://floss.social/@ansol).

Temos uma sala Matrix aberta ao público: <https://matrix.to/#/#geral:ansol.org>
