---
categories:
- imprensa
- drm
metadata:
  tags:
  - tags_tid: 19
  - tags_tid: 10
  node_id: 500
layout: article
title: Parlamento aprova projeto de lei que resolve DRM
created: 1491686727
date: 2017-04-08
aliases:
- "/article/500/"
- "/node/500/"
- "/proposta-DRM-aprovada/"
---
<p><img src="https://ansol.org/attachments/no-more-locks.jpg" width="800" height="568" style="display: block; margin-left: auto; margin-right: auto;"></p><p style="text-align: center;"><span style="font-size: x-small;"><em>Imagem por Paula Simões, disponível com uma licença CC-BY</em></span></p><p>Foi ontem aprovado o projeto de lei do Bloco de Esquerda, que garante&nbsp;aos cidadãos poderem realizar as utilizações livres, mesmo que as obras tenham DRM. Falamos da utilização para fins de ensino e de investigação científica, da cópia privada, entre outras cujas condições de utilização podem ser verificadas no <a href="http://www.pgdlisboa.pt/leis/lei_mostra_articulado.php?artigo_id=484A0075&amp;nid=484&amp;tabela=leis&amp;pagina=1&amp;ficha=1&amp;so_miolo=&amp;nversao=#artigo" target="_blank">artigo 75º</a> e seguintes do Código de Direito de Autor e Direitos Conexos.</p><p>Depois de trabalhado e aprovado na <a href="http://www.parlamento.pt/sites/com/XIIILeg/12CCCJD/Paginas/default.aspx" target="_blank">Comissão de Cultura, Comunicação, Juventude e Desporto</a>, o projeto foi aprovado em plenário com os votos a favor dos grupos parlamentares do BE, PS, PCP, Verdes, PAN, com a abstenção do grupo parlamentar do CDSPP e os votos contra do grupo parlamentar do PSD.</p><p>Para além de permitir a realização das utilizações livres, o projeto de lei interdita&nbsp;ainda a colocação de DRM em obras caídas no domínio público (que já não têm direitos de autor patrimoniais), protegendo o nosso património cultural, bem como interdita ainda a colocação de DRM em obras editadas por&nbsp;entidades públicas ou com financiamento público.</p><p>A&nbsp;<a href="https://ansol.org/" target="_blank">ANSOL</a> e a <a href="http://ensinolivre.pt">Associação Ensino Livre</a>, dinamizadores do <a href="https://drm-pt.info" target="_blank">movimento DRM-PT</a>, congratulam o Bloco de Esquerda, o Partido Socialista, o Partido Comunista Português, o Partido Ecologista Os Verdes e o Partido Pessoas, Animais e Natureza pela inciativa, trabalho e apoio em garantir que os cidadãos possam finalmente exercer os seus direitos fundamentais também no que respeita a obras com DRM.</p><p>O projeto de lei aprovado terá ainda de ser promulgado pelo Sr. Presidente da República.</p><p>O texto final pode ser <a href="http://app.parlamento.pt/webutils/docs/doc.pdf?path=6148523063446f764c324679626d56304c334e706447567a4c31684a53556c4d5a5763765130394e4c7a457951304e44536b51765247396a6457316c626e527663306c7561574e7059585270646d46446232317063334e68627938774f474a694e6a41784f53307959325a684c545135596a5174596a67355969316a4e7a466c596a466d4d6a49354d5749756347526d&amp;fich=08bb6019-2cfa-49b4-b89b-c71eb1f2291b.pdf&amp;Inline=true" target="_blank">consultado neste link</a> [PDF].</p>
