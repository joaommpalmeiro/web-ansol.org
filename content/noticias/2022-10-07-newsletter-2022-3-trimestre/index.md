---
categories:
- newsletter
layout: article
title: Newsletter 2022 - 3º trimestre
date: 2022-10-07
---

No 3º trimestre de 2022 a ANSOL organizou um evento a celebrar o Dia do
Software Livre, participou num painel e organizou um workshop nas IV Jornadas
Open Source, e organizou várias sessões de tradução de Software Livre. Também
nos tornámos associados da [PASC](https://pasc.pt) e assinámos protocolos com
dois Agrupamentos de Escolas.

<!--more-->

## Sessões de Tradução de Software Livre

Começámos uma iniciativa de organizar sessões de tradução de projectos de
Software Livre. Este trimestre organizámos três sessões:

- Introdução ao tema e a ferramentas de tradução
- [Sessão conjunta de traduções Mozilla](https://ansol.org/eventos/2022-07-19-traducoes-mozilla/)
- [Sessão conjunta de traduções Nextcloud](https://ansol.org/eventos/2022-08-11-traducoes-nextcloud/)

Para quem tiver interesse no tema, mantemos uma sala Matrix aberta ao público
sobre este tópico: [#traduções:ansol.org](trad-matrix). Também nos podem seguir
nas redes sociais, onde divulgaremos as próximas sessões.


## IV Jornadas Open Source

Estivemos presentes nas [IV Jornadas Open Source][jornadas], organizadas pela
[BAD][bad]. Neste evento, o Tiago Carrondo participou num painel intitulado
"Open Source: transição, capacitação e sustentabilidade" e organizou um
workshop sobre Hugo: "Criação e publicação de websites de baixa manutenção e
alta preservação usando Software Livre".


## Associação à PASC

A ANSOL tornou-se associada da [PASC][pasc]. A PASC faz parte da Rede Nacional
de Administração Aberta, criada para coordenar a participação portuguesa na
[OGP][ogp].


## Protocolos com Agrupamentos de Escolas

Assinámos protocolos de parceria com o Agrupamento de Escolas de Santa Maria da
Feira e com o Agrupamento de Escolas Augusto Cabrita, no âmbito do Plano de
Recuperação e Resiliência.


## Dia do Software Livre 2022

Para celebrar o [Dia do Software Livre 2022][sfd], organizámos um evento
presencial no Porto com a parceria do [Espaço Musas][musas], que gentilmente
nos disponibilizou o seu espaço. Tivemos três apresentações (com muita conversa
à mistura):

- "Governança Open Source no Ember.js" por Ricardo Mendes
- "Software Livre: pagar para quê?" por Rui Teixeira
- "O Open Source ganhou. O Software Livre perdeu. E agora?" por Ricardo Lafuente

Os vídeos das apresentações estão disponíveis em <https://viste.pt/w/p/4irPoTqG4bSHPU4JD56TGL>


## Segue as actividades da ANSOL

Podem seguir as actividades através da nossa presença em várias redes:

- Segue-nos no Mastodon: <https://floss.social/@ansol>
- Segue-nos no Peertube: <https://viste.pt/c/ansol/>
- Segue-nos no Twitter: <https://twitter.com/ANSOL>
- Junta-te à nossa sala via Matrix: <https://matrix.to/#/#geral:ansol.org>
- Vê os nossos repositórios de Software Livre: <https://git.ansol.org/ansol>

Se consideras importante o tema de Software Livre na sociedade, considera
[juntar-te à ANSOL](https://ansol.org/inscricao/). A ANSOL depende
exclusivamente da contribuição dos seus membros para suportar as suas
actividades, e gostaríamos de contar com a tua participação.



[sfd]: https://www.softwarefreedomday.org/
[musas]: http://musas.pegada.net/
[bad]: https://bad.pt
[jornadas]: https://eventos.bad.pt/event/iv-jornadas-de-open-source/
[pasc]: https://pasc.pt/
[ogp]: https://ogp.eportugal.gov.pt
[trad-matrix]: https://matrix.to/#/#traduções:ansol.org
