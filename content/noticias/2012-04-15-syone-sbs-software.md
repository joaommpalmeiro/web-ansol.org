---
categories:
- consultoria
- distribuição/venda
- formação
- suporte
metadata:
  email:
  - email_email: marketing@syone.com
  servicos:
  - servicos_tid: 7
  - servicos_tid: 8
  - servicos_tid: 3
  - servicos_tid: 2
  site:
  - site_url: http://www.syone.com
    site_title: http://www.syone.com
    site_attributes: a:0:{}
  node_id: 49
layout: servicos
title: Syone SBS Software
created: 1334501101
date: 2012-04-15
aliases:
- "/node/49/"
- "/servicos/49/"
---
<h2>
	Consultoria</h2>
<p>A Syone disponibiliza todo o portfolio de produtos e servi&ccedil;os certificados Red Hat e JBoss: desde as solu&ccedil;&otilde;es empresariais at&eacute; &agrave; consultoria, suporte, desenvolvimento &agrave; medida, Red Hat Network, JBoss e outros servi&ccedil;os e solu&ccedil;&otilde;es de valor acrescentado, incluindo o servi&ccedil;o de suporte local LocalSupport24, com SLA 24x7.</p>
<h2>
	Distribui&ccedil;&atilde;o/Venda</h2>
<p>Com base na sua forte aposta e larga experi&ecirc;ncia no mercado Linux, a Syone foi nomeada Red Hat Premier Partner.</p>
<p>Em Portugal somos a primeira e &uacute;nica empresa com este n&iacute;vel de parceria, e com o maior n&uacute;mero de consultores acreditados e certificados pela Red Hat.</p>
<p>Como Red Hat Premier Partner, a Syone disponibiliza todo o portfolio de produtos e servi&ccedil;os certificados Red Hat &ndash; desde as solu&ccedil;&otilde;es at&eacute; &agrave; consultoria, suporte, desenvolvimento &agrave; medida, Red Hat Network, JBoss e outros produtos de valor acrescentado, nomeadamente:</p>
<ul>
	<li>
		Red Hat Enterprise Linux (RHEL), Sistema Operativo Empresarial</li>
	<li>
		Red Hat Network Satellite, Plataforma de Gest&atilde;o de Infraestrutura</li>
	<li>
		Red Hat Enterprise Virtualization (RHEV), Software de Virtualiza&ccedil;&atilde;o</li>
	<li>
		Red Hat JBoss, Middleware Aplicacional</li>
</ul>
<p>Para todas estas solu&ccedil;&otilde;es a Syone oferece todo um conjunto de Servi&ccedil;os e Consultores certificados com experi&ecirc;ncia comprovada em projectos nacionais e internacionais.</p>
<h2>
	Forma&ccedil;&atilde;o</h2>
<p>Tendo detido at&eacute; 2009 o estatuto de Certified Training Center Red Hat, a Syone continua a disponibilizar consultoria de forma&ccedil;&atilde;o nas solu&ccedil;&otilde;es e tecnologias Red Hat e JBoss, em instala&ccedil;&otilde;es pr&oacute;prias e/ou em casa do Cliente.</p>
<h2>
	Suporte</h2>
<p>Como &uacute;nica empresa Red Hat Premier Business Partner existente em Portugal, a Syone disponibiliza todo o portfolio de produtos e servi&ccedil;os certificados Red Hat &ndash; desde as solu&ccedil;&otilde;es at&eacute; &agrave; consultoria, suporte, desenvolvimento &agrave; medida, Red Hat Network, JBoss e outros produtos de valor acrescentado,&nbsp; incluindo o servi&ccedil;o de suporte local LocalSupport24, com SLA 24x7.</p>
