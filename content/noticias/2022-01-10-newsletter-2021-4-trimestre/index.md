---
categories:
- newsletter
layout: article
title: Newsletter 2021 - 4º trimestre
date: 2022-01-10
---

No 4º trimestre de 2021 a ANSOL desenvolveu um conjunto de acções e
actividades, em várias áreas, e cumpriu algumas tradições próprias desta altura
do ano.

<!--more-->

* Para celebrar o seu 20º aniversário, organizámos um ciclo de apresentações
  onde foram analisados os últimos 20 anos do Software Livre em Portugal e
  discutidos os próximos 20 anos. Todo o programa, incluíndo uns vídeos extra,
  pode [ser visto no site da associação](https://ansol.org/noticias/2021-09-12-ansol-20-anos-de-software-livre/).
* Criámos dois canais oficiais da ANSOL no Matrix. Um aberto ao público em
  geral: [#geral:ansol.org](https://matrix.to/#/#geral:ansol.org), e um para
  sócios: [#socios:ansol.org](https://matrix.to/#/#socios:ansol.org);
* No âmbito do [Hacktoberfest 2021](https://hacktoberfest.digitalocean.com/),
  organizámos uma hackathon durante a última semana do mês;
* Refizemos o nosso [website institucional](https://ansol.org);
* Houve a Assembleia Geral 2021 Ordinária;
* Reunimos com representantes do partido VOLT para apresentar as ideias da
  ANSOL para a legislatura de 2022.

Agradecemos publicamente aos novos sócios que aderiram recentemente à
associação, e com quem estamos ansiosos por colaborar:

* J. M.
* F. M.
* C. C.
* R. T.
* M. C.
* C. C.
* P. P.
