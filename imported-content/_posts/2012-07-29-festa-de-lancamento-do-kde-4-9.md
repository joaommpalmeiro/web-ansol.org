---
categories: []
metadata:
  event_location:
  - event_location_value: Restaurante Lá Rúcula, Parque das Nações, Lisboa
  event_site:
  - event_site_url: http://community.kde.org/Promo/Events/Release_Parties/4.9#Portugal
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-08-01 18:30:00.000000000 +01:00
    event_start_value2: 2012-08-01 22:00:00.000000000 +01:00
  node_id: 82
layout: evento
title: Festa de Lançamento do KDE 4.9
created: 1343521026
date: 2012-07-29
---
<p>A ANSOL[1] e a AEL[2] juntam-se para celebrar o lan&ccedil;amento do KDE[3] 4.9, numa de muitas celebra&ccedil;&otilde;es que ir&atilde;o ocorrer um pouco por todo o mundo[4].</p>
<p>O jantar de celebra&ccedil;&atilde;o ser&aacute; no restaurante La R&uacute;cula, no Parque das Na&ccedil;&otilde;es em Lisboa.</p>
<p>A presen&ccedil;a no jantar n&atilde;o &eacute; sujeita a confirma&ccedil;&atilde;o, mas agradece-se, contudo, se puderem confirmar, enviando um mail para marcos.marado em ansol.org.</p>
<p>&nbsp;</p>
<p>Mais informa&ccedil;&atilde;o em http://community.kde.org/Promo/Events/Release_Parties/4.9#Portugal</p>
<p>&nbsp;</p>
<p>[1] ANSOL - Associa&ccedil;&atilde;o Nacional para o Software Livre - https://ansol.org</p>
<p>[2] AEL - Associa&ccedil;&atilde;o Ensino Livre - http://ensinolivre.pt</p>
<p>[3] KDE - http://kde.org</p>
<p>[4] http://community.kde.org/Promo/Events/Release_Parties/4.9</p>
