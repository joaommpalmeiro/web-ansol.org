---
categories:
- java
metadata:
  event_site:
  - event_site_url: http://www.meetup.com/Coimbra-JUG/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-12-11 00:00:00.000000000 +00:00
    event_start_value2: 2014-12-11 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 55
  node_id: 255
layout: evento
title: Quick Web Applications Development using JBoss Forge
created: 1418224984
date: 2014-12-10
---
<div><strong><span style="font-size: large;"><span>No próximo dia 11 de Dezembro, o Coimbra JUG irá realizar o seu 7º encontro, com uma sessão dedicada ao tema: "Quick Web Applications Development using JBoss Forge" tendo como orador&nbsp;internacional,&nbsp;</span><span>Koen Aers.</span></span></strong></div><div><strong><span style="font-size: large;"><span>&nbsp;</span></span></strong></div><div><strong><span style="font-size: large;"><span>O encontro terá lugar no Departamento de Engenharia Informática (DEI-FCTUC), pelas 18h15m no Anfiteatro B1.</span></span></strong></div><div><strong><span style="font-family: Calibri; font-size: large;"><em>&nbsp;</em></span></strong></div><div><em><span style="font-family: Calibri; font-size: large;">Para mais informações e inscrição no grupo, consultar&nbsp;<a href="http://www.meetup.com/Coimbra-JUG/" target="_blank" style="color: #196ad4;" rel="nofollow">http://www.meetup.com/Coimbra-JUG/</a>.</span></em></div><div>&nbsp;</div><div><div dir="ltr"><em><span style="font-family: Calibri; font-size: large;">Para todos aqueles que partilham uma paixão pela linguagem de programação Java e tecnologias associadas, foi criado o Coimbra JUG (Java User Group). O objectivo é construir uma comunidade na zona de Coimbra e Zona Centro que permita a troca de ideias, experiências e conhecimentos entre os vários membros do grupo.&nbsp;</span></em><em><span style="font-family: Calibri; font-size: large;">Se és um entusiasta de Java, junta-te e ajuda a comunidade a crescer!</span></em></div></div>
