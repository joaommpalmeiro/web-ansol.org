---
categories: []
metadata:
  event_location:
  - event_location_value: Parque de Exposições de Aveiro
  event_site:
  - event_site_url: http://www.techdays.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-09-14 23:00:00.000000000 +01:00
    event_start_value2: 2016-09-16 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 443
layout: evento
title: Techdays Aveiro
created: 1472500123
date: 2016-08-29
---
<p>O Techdays Aveiro é um evento de três dias, onde se falará um pouco de tudo sobre tecnologia.</p><p>Encontra a ANSOL no espaço expositivo durante o evento - estaremos junto dos nossos amigos da <a href="http://hackaveiro.org/">Hack'Aveiro</a>!</p>
