---
categories: []
metadata:
  event_location:
  - event_location_value: Universidade Portucalense, Porto
  event_site:
  - event_site_url: http://capsi2003.upt.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2003-09-16 23:00:00.000000000 +01:00
    event_start_value2: 2003-09-16 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 187
layout: evento
title: 4ª Conferência da Associação Portuguesa de Sistemas de Informação
created: 1371948968
date: 2013-06-23
---
<p>Jaime Villate (ANSOL) foi convidado a intervir numa sessão plenária sobre software livre, onde estarão presentes representates do INA e da Microsoft.</p>
