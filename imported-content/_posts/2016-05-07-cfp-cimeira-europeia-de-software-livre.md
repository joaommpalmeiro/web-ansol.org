---
categories:
- cfp
- fsfe
- cimeira
metadata:
  tags:
  - tags_tid: 169
  - tags_tid: 63
  - tags_tid: 170
  node_id: 419
layout: article
title: 'CfP: Cimeira Europeia de Software Livre'
created: 1462649135
date: 2016-05-07
---
<p>"Imagine uma União Europeia que constrói a sua infraestrutura de IT com Software Livre. Imagine Estados Membros que trocam informação usando Normas Abertas e que partilham o seu software. Imagine municípios e assembleias municipais que benificiam de software descentralizado e&nbsp; colaborativo com licenças livres. Imagine que nenhum Europeu continua a ser forçado a usar Software não-Livre.<br><br>Esta é o cenário que procuramos. E apesar desta visão parecer longínqua, sabemos que estamos a tomar passos largos nesse caminho. Para ajudar a desbloquear todo o nosso potencial nesse caminho, a Free Software Foundation Europe (FSFE) oferece uma colaboração além-fronteiras e a sua primeira cimeira Europeia. Faça parte dela e junte-se ao nosso movimento."<br><br>Em 2016 a FSFE celebra 15 anos de existência com a sua primeira Cimeira Europeia. Esta cimeira, a decorrer de 2 a 4 de Setembro, no centro de<br>congressos de Berlim, abre agora a sua "chamada à participação". <br><br>Assim, se estiverem interessados em participar com apresentações, workshops, voluntariado, moderação, etc., vejam mais informação em<br><a href="https://wiki.fsfe.org/Events/Summit2016/CallForParticipation">https://wiki.fsfe.org/Events/Summit2016/CallForParticipation</a>, até 17 de Maio.<br><br>Participem!<br><br></p>
