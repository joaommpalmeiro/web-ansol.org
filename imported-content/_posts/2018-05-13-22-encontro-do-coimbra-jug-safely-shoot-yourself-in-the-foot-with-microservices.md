---
categories:
- java
- microservices
- user group
- coimbra
metadata:
  event_location:
  - event_location_value: Praxis Coimbra - R. António Gonçalves, Lote 28/29, Santa
      Clara · Coimbra, Portugal
  event_site:
  - event_site_url: https://www.meetup.com/Coimbra-JUG/events/250175880/?rv=me1&_xtd=gatlbWFpbF9jbGlja9oAJDVkMmRjYzM1LWU4YWEtNDdjOS1hNzIwLTIwYzg4MjllNjlhYg&_af=event&_af_eid=250175880&https=on
    event_site_title: Safely Shoot Yourself in the Foot with Microservices
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-17 17:30:00.000000000 +01:00
    event_start_value2: 2018-05-17 19:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 55
  - tags_tid: 312
  - tags_tid: 158
  - tags_tid: 55
  - tags_tid: 233
  node_id: 621
layout: evento
title: 22º encontro do Coimbra JUG -  Safely Shoot Yourself in the Foot with Microservices
created: 1526251311
date: 2018-05-13
---
<div class="chunk event-description--wrapper"><div class="event-description runningText"><p>No 22º encontro do Coimbra JUG, teremos o prazer de contar com mais um orador internacional e colega do Vigo JUG e Corunha JUG: o Antón Yuste (<a href="https://twitter.com/antonmry" target="__blank" title="https://twitter.com/antonmry" class="link">https://twitter.com/antonmry</a>). O Antón vai apresentar uma sessão sobre Microservices, uma arquitectura para desenvolver aplicações popularizada pela Netflix.<br><br>Não deixem de aparecer!<br><br>Safely Shoot Yourself in the Foot with Microservices<br><br>Microservices architecture has a lot of attention nowadays and it’s started to be used by very big enterprises which want to shorten time to market by improving productivity effect through maximizing the automation in all life circle of their services. However, microservices architecture approach also introduces a lot of new complexity and requires application developers a certain level of maturity in order to success. The approach is very different depending of the size, maturity and type of enterprise.<br>In this talk, Anton will share his experience deploying microservices in some big telecom operators: the main differences with the same architecture in startups, how to take the advantage of the JVM, what are the most common mistakes and what should be avoid at all cost.<br><br>Bio: Antón Yuste (@antonmry (<a href="https://twitter.com/antonmry" target="__blank" title="https://twitter.com/antonmry" class="link">https://twitter.com/antonmry</a>))<br><br>Antón is a Software Engineer who specializes in JVM development, performance tuning, and distributed systems, mainly in the telecom sector. In his current role as Technical Director in Optare, he helps to define the technological strategy in conjunction with the development team of each project: pipeline, tools, and key development procedures.<br>He also co-organizes the Vigo and Coruña Java User Groups (VigoJUG &amp; CoruñaJUG) and sometimes he speaks at international conferences as Geecon (Krakow), SpringIO (Barcelona), TADSummit (Istanbul), Librecon (Santiago de Compostela), etc.</p></div></div><div class="attendees-sample"><div class="flex flex--row"><div class="flex-item"><h3 class="attendees-sample-total text--sectionTitle text--bold padding--bottom"><span>&nbsp;</span></h3></div></div></div>
