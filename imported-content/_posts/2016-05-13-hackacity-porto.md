---
categories: []
metadata:
  event_site:
  - event_site_url: http://porto.hackacity.eu/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-05-26 23:00:00.000000000 +01:00
    event_start_value2: 2016-05-27 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 421
layout: evento
title: Hackacity Porto
created: 1463154157
date: 2016-05-13
---
<div id="element-custom-block-129983346" class="element "><div id="sfid-129983346" class="sf content headline "><div class="title grp p0"><p>Uma nova edição do épico hackathon que tem como objetivo melhorar a vida de quem vive ou visita o Porto está a chegar.<br>Uma maratona de 24 horas de trabalho em equipa com algumas surpresas... e acima de tudo muita diversão e aprendizagem!</p></div></div></div>
