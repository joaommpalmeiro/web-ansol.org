---
categories: []
metadata:
  event_location:
  - event_location_value: Parlamento
  event_start:
  - event_start_value: 2015-05-07 23:00:00.000000000 +01:00
    event_start_value2: 2015-05-07 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 311
layout: evento
title: Cópia Privada no Parlamento
created: 1429730952
date: 2015-04-22
---
<p>No dia 8 de Maio é discutida a reapreciação do decreto de lei da cópia privada, que foi vetado pelo Presidente da República, em simultâneo com a discussão da petição contra aquele diploma.</p>
