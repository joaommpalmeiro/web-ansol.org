---
categories: []
metadata:
  event_location:
  - event_location_value: Universidade de Aveiro
  event_site:
  - event_site_url: http://www.bad.pt/form/index.php?option=com_rsform&formId=112
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-10-16 23:00:00.000000000 +01:00
    event_start_value2: 2016-10-16 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 466
layout: evento
title: II Jornadas Open Source
created: 1476044421
date: 2016-10-09
---
<p>As II Jornadas Open Source, dinamizadas pela Delegação Regional Norte e Centro da Associação Portuguesa de Bibliotecários, Arquivistas e Documentalistas (BAD), com o apoio da Universidade de Aveiro (UA) realizam-se dia 17 de outubro na UA. Estas Jornadas pretendem facultar a todos os profissionais de informação um espaço de debate, de partilha de experiências e de aquisição de novos conhecimentos em torno de Sistemas de Informação Open Source para Arquivos, Bibliotecas e Museus.</p><p>Participam na iniciativa a Universidade de Aveiro, a Universidade do Porto, a Universidade Fernando Pessoa, a Universidade de Lisboa, o Museu de Ciência da Universidade de Coimbra, o Arquivo&nbsp;Municipal de Albergaria-a-Velha, a pPorto.pt, a Sistemas do Futuro, entre outras.</p><p><strong>Programa provisório</strong></p><p>08h45&nbsp;– Registo e distribuição de documentação</p><p>09h15&nbsp;– Sessão de abertura Ana Bela Martins&nbsp;| Diretora dos Serviços de Biblioteca, Informação Documental e Museologia da Universidade de Aveiro Isabel Costa&nbsp;| Presidente BAD-DR Norte e Andrea Martins | Vogal BAD-DR Centro</p><p>09h30 – 11h15 | Painel 1 – O CONTEXTO Rui Raposo&nbsp;–&nbsp;“Antes, durante e depois: Alguns desafios do presente e futuro em instituições que trabalham sobre o passado”. (Universidade de Aveiro) Patrícia Remelgado&nbsp;–&nbsp;“Sistemas de informação e Comunicação em Museus: uma realidade orgânica”. (Porto dos Museus) Luís Borges Gouveia&nbsp;–&nbsp;“O digital, a sustentabilidade e a viagem do open source ao open data”. (Universidade Fernando Pessoa) Luís Corujo&nbsp;–&nbsp;“Repositórios Digitais e Certificação: o RODA como exemplo”. (Universidade de Lisboa).</p><p>11h15 – 11h30&nbsp;–CoffeBreak</p><p>11h30- 13h00 | Painel 2 – AS EXPERIÊNCIAS Hélder Silva&nbsp;–&nbsp;“A implementação do AtoM no Município de Albergaria-a-Velha”. (Município de Albergaria) Pedro Casaleiro&nbsp;e&nbsp;Alexandre Matos&nbsp;–&nbsp;“Museu Digital e SPECTRUM”. (Museu da Ciência da Univerdade de Coimbra e Sistemas do Futuro) (convidado a designar)</p><p>13h00 – 14h00&nbsp;– Almoço Livre</p><p>14h00 | Painel 3 – O ACESSO INTEGRADO 14h15 – 15h45&nbsp;–&nbsp;“Open Source trocado por miúdos”. (Serviços de Biblioteca, Informação Documental e Museologia e Serviços de Tecnologias da Informação e Comunicação | Universidade de Aveiro)</p><p>15h45 – 16h00&nbsp;–Coffebreak</p><p>16h00 – 17h30&nbsp; Susana Medina,&nbsp;Maria Manuela Pinto&nbsp;e&nbsp;Augusto Ribeiro&nbsp;–“Museu Digital U. Porto: contributos para a gestão holística e integrada do património científico-cultural”. (Universidade do Porto)</p><p>17h30&nbsp;– Resumo da jornada</p><p>18h00&nbsp;– Encerramento</p>
