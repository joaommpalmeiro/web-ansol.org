---
categories: []
metadata:
  event_location:
  - event_location_value: Congresso do Brasil, Brasilia
  event_start:
  - event_start_value: 2003-08-17 23:00:00.000000000 +01:00
    event_start_value2: 2003-08-21 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 186
layout: evento
title: Semana Software Livre no Legislativo
created: 1371948838
date: 2013-06-23
---
<p>Jaime Villate (ANSOL) fez uma apresentação sobre "Uma Visão Institucional Internacional sobre o Software Livre" no dia 19.</p>
