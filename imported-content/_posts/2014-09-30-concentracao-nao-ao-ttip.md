---
categories: []
metadata:
  event_location:
  - event_location_value: Rossio, Lisboa
  event_site:
  - event_site_url: http://www.nao-ao-ttip.pt/manifestacao-rossio/#more-25
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-10-11 16:00:00.000000000 +01:00
    event_start_value2: 2014-10-11 19:15:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 232
layout: evento
title: 'Concentração: Não ao TTIP'
created: 1412081712
date: 2014-09-30
---
<div style="float: left; padding: 1em;" align="left"><img src="http://www.nao-ao-ttip.pt/wp-content/uploads/2014/10/flyer_A4-manif-11-10-217x300.jpg" alt="poster" width="217" height="300"></div><p>A 11 de outubro, a Plataforma Não ao TTIP irá organizar um evento público de divulgação sobre o TTIP (Tratado Transatlântico) no Rossio, pelas 17:00.<br> Está convidado a participar nesta ação e a integrar esta plataforma de cidadãos e de organizações da sociedade civil.</p><p>&nbsp;</p><h3>NÃO AO TTIP, AO CETA E AO TISA</h3><p>Vamos exigir a democracia e sacudir o poder das corporações! Os povos e o planeta têm de vir antes dos lucros. 11 de Outubro de 2014, dia europeu de acção (descentralizada) para rejeitar os tratados TTIP, CETA e TISA.</p><h3>Juntem-se a nós!</h3><p>A sociedade civil, os sindicatos, os grupos e associações de agricultores e activistas em toda a Europa, preparam um dia de acção contra os tratados TTIP, CETA, TISA e outros ditos de livre comércio, exigindo políticas alternativas que coloquem as pessoas, os direitos, a democracia e o ambiente em primeiro lugar. O TTIP (Transatlantc Trade and Investement Partnership) e o CETA (Comprehensive Economic and Trade Agreement entre o Canadá e a EU) são os mais proeminentes exemplos de como são negociadas as políticas de comércio e investimento. As negociações totalmente anti-democráticas são conduzidas apenas com vista ao lucro das grandes corporações. Tudo é feito em segredo e é muito escassa a informação que chega ao público, enquanto as corporações e os seus lobbies aumentam a sua influência sobre elas.</p><p>Se estes acordos forem avante, as corporações multinacionais terão direitos exclusivos sobre os governos, podendo processá-los em tribunais especiais, independentes dos sistemas legais nacionais e europeus. Vão reduzir os padrões de saúde e segurança a caminho da “harmonização” transatlântica, ao mesmo tempo que minam a autoridade dos governos locais e nacionais que pretendam prevenir práticas lesivas como o fracking ou o uso de alimentos geneticamente modificados (OGM). Vão também forçar a venda ao desbarato dos serviços públicos essenciais e obrigar os trabalhadores e os direitos sociais a uma corrida para o abismo. Estas políticas estão estreitamente ligadas ao desmantelamento em curso do estado social e à pressão para privatizar os serviços públicos em nome da austeridade, das políticas da crise e da “competitividade”.</p><p>O dia da acção tornará visível o nosso descontentamento público nas ruas de toda a Europa. Levaremos o debate destas questões à arena pública, de onde a UE, a Comissão e os governos querem manter afastado. Iremos promover as nossas alternativas. Defendemos a solidariedade entre os cidadãos e grupos do mundo que partilham a nossa preocupação sobre o ambiente, a democracia e os direitos humanos e laborais. O TTIP, o CETA e o TISA serão impedidos pela energia dos cidadãos europeus do Canadá e dos EUA que começam a fazer ouvir a sua voz.</p><p>Apelamos às organizações, aos grupos e aos indivíduos para participarem e organizarem acções autónomas descentralizadas por toda a Europa. Apoiamos acções e tácticas diversas pelo mundo fora que ajudem a informar organizar e mobilizar os cidadãos e as comunidades locais.</p><p>Vamos ganhar esta batalha. Juntos podemos reverter o poder das corporações. Os grupos poderão escolher outro dia se o considerarem mais apropriado.</p><p>O dia 11 de Outubro é também o dia global contra o fracking.</p>
