---
categories:
- cyanogen
- android
- drink-up
- social
- encontro
metadata:
  event_location:
  - event_location_value: Bar entretanto, Hotel do Chiado, Lisboa
  event_site:
  - event_site_url: http://www.meetup.com/Android-LX/events/220892296
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-03-09 19:00:00.000000000 +00:00
    event_start_value2: 2015-03-09 19:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 77
  - tags_tid: 78
  - tags_tid: 79
  - tags_tid: 80
  - tags_tid: 74
  node_id: 290
layout: evento
title: Cyanogen Drink-Up!
created: 1425393837
date: 2015-03-03
---
<div style="float: left; padding: 1em;"><img src="https://ansol.org/sites/ansol.org/files/cyngnpt.jpg" alt="Poster" height="600" width="424"></div><p>As equipas da <strong>Cyanogen</strong> e do <strong>Android LX</strong> convidam a comunidade Android para partilhar algumas bebidas e aperitivos no dia <strong>9 de Março</strong> no bar panorâmico do <a href="https://www.zomato.com/pt/grande-lisboa/entretanto-hotel-do-chiado-chiado-lisboa">Hotel do Chiado, Bar Entretanto</a>!</p><p>Vem, e discute tópicos tecnológicos relacionados com Android com outros programadores, hackers, designers e utilizadores. Com o convidado especial <strong>Steve Kondik</strong>, Fundador e CTO da <a href="https://cyngn.com/">Cyanogen</a> (<a href="https://cyngn.com" class="linkified">https://cyngn.com</a>) e outros membros da equipa Cyanogen.</p>
