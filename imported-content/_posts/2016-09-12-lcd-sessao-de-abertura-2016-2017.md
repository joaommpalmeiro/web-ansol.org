---
categories: []
metadata:
  event_location:
  - event_location_value: Laboratório de Criação Digital
  event_site:
  - event_site_url: https://www.facebook.com/events/1108024275944062/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-09-20 20:00:00.000000000 +01:00
    event_start_value2: 2016-09-20 22:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 451
layout: evento
title: 'LCD: Sessão de Abertura 2016-2017'
created: 1473685237
date: 2016-09-12
---
<p><span class="_4n-j fsl">Para marcar a abertura de mais um ano de atividades do "Laboratório de Criação Digital" (LCD), é promovida uma sessão especial do Laboratório Aberto em que serão apresentadas as atividades e projetos que serão desenvolvidas entre Setembro a Dezembro.</span></p><p style="margin-left: 30px;"><span class="_4n-j fsl"><span class="_4n-j fsl"><strong> Programa:</strong> <br> 21h00 - Abertura do espaço <br> 21h05 - Comer e falar<br> 21h30 - Plano de atividades<br> 22h30 - Inauguração do novo site<br> 23h00 - Project-based learning</span></span></p>
