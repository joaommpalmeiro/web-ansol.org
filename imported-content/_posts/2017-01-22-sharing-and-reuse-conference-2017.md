---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://joinup.ec.europa.eu/node/158329/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-03-28 23:00:00.000000000 +01:00
    event_start_value2: 2017-03-28 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 480
layout: evento
title: Sharing & Reuse Conference 2017
created: 1485104053
date: 2017-01-22
---

