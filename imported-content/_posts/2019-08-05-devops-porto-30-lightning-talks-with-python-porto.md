---
categories: []
metadata:
  event_location:
  - event_location_value: Euronext Tech Center Porto
  event_site:
  - event_site_url: https://www.meetup.com/pt-BR/devopsporto/events/263166374/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-08-08 17:45:00.000000000 +01:00
    event_start_value2: 2019-08-08 17:45:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 687
layout: evento
title: 'DevOps Porto #30: Lightning Talks with Python Porto'
created: 1565000457
date: 2019-08-05
---

