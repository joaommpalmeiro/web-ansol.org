---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://meta.wikimedia.org/wiki/Festa_da_Wiki-Lusofonia/Eventos/Sess%C3%A3o_principal
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-05-07 23:00:00.000000000 +01:00
    event_start_value2: 2021-05-08 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 798
layout: evento
title: 'Mesa redonda: O impacto da Wikipédia no mundo da cultura livre | Festa da
  Wiki-Lusofonia'
created: 1619637877
date: 2021-04-28
---
<p>A ANSOL participou nesta Mesa Redonda, que pode ser agora vista em diferido:</p><p><iframe title="YouTube video player" src="https://www.youtube.com/embed/pevyfo9Rwhg" width="560" height="315" frameborder="0"></iframe></p>
