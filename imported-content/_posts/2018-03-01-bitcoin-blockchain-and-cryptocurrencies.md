---
categories:
- bitcoin
- viana de castelo
- blockchain
metadata:
  event_location:
  - event_location_value: PICU's Office - Largo de S.Domingo, 52, 1ºandar, Viana de
      Castelo
  event_site:
  - event_site_url: https://www.meetup.com/VianaTechMeetups/events/248127988/
    event_site_title: Bitcoin, Blockchain & Cryptocurrencies
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-03-22 18:00:00.000000000 +00:00
    event_start_value2: 2018-03-22 20:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 214
  - tags_tid: 215
  - tags_tid: 209
  node_id: 549
layout: evento
title: Bitcoin, Blockchain & Cryptocurrencies
created: 1519935702
date: 2018-03-01
---
<p>As Viana Tech Meetups estão a crescer e agitar o panorama da cidade graças a esta comunidade fantástica! Desta forma vimos a oportunidade de trazer ainda mais espaços de networking e aprendizagem, desta vez num formato de troca de experiências e conversa em grupo.<br><br>E é assim que nasceram as US Sessions!<br><br>As US Sessions trazem o melhor destes meetups: um ambiente informal e de boa disposição, onde qualquer pessoa pode se juntar para discutir o tema proposto. Estas sessões ocorrerão no escritório da PICUS em Viana e têm um número limitado de entrada.<br><br>Para esta US Session, vamos abordar toda esta informação com que temos sido confrontados maioritariamente desde de 2017: Bitcoin, Blockchain, e Cryptocurrencies. Nós sabemos que por Viana e arredores existem fanáticos, evangelistas, investidores, newbies e dummies nesta área. Por isso mesmo queremos criar uma oportunidade para partilhar as nossas experiências e/ou simplesmente aprender algo mais sobre este mundo novo!<br><br>Ficamos à tua espera na PICUS e já agora não te esqueças de seguir o melhor site nacional sobre Bitcoin, Blockchain e Crypto! <a href="https://bitcoinhoje.pt/" target="__blank" title="https://bitcoinhoje.pt/" class="link">https://bitcoinhoje.pt/</a><br><br>18:00: Chegadas e boas vindas<br>18:30: Partilha de experiências e networking<br>20:00: Jantar (opcional)<br><br>Queres fazer parte da melhor comunidade tech de Viana? Subscreve aqui! <a href="http://bit.ly/2nFSDk0" target="__blank" title="http://bit.ly/2nFSDk0" class="link">http://bit.ly/2nFSDk0</a><br><br>Food &amp; drinks do Mercado na Loja<br><br>Largo São Domingos 52 1º Andar Viana do Castelo, Portugal 4900-330<br><br><a href="https://picuscreative.com" target="__blank" title="https://picuscreative.com" class="link">https://picuscreative.com</a><br>Facebook: <a href="https://www.facebook.com/picuscreative/" target="__blank" title="https://www.facebook.com/picuscreative/" class="link">https://www.facebook.com/picuscreative/</a><br>Follow no instagram: <a href="https://www.instagram.com/picuscreative/" target="__blank" title="https://www.instagram.com/picuscreative/" class="link">https://www.instagram.com/picuscreative/</a><br>Youtube channel: <a href="http://bit.ly/SUB2PICUS" target="__blank" title="http://bit.ly/SUB2PICUS" class="link">http://bit.ly/SUB2PICUS</a></p>
