---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://ec.europa.eu/eusurvey/runner/OSORCommunityEvent-Sustainability
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-05-17 23:00:00.000000000 +01:00
    event_start_value2: 2021-05-17 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 799
layout: evento
title: Sustainability of Open Source Communities
created: 1620853375
date: 2021-05-12
---

