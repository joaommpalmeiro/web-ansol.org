---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: http://groups.drupal.org/node/295708#comment-918093
    event_site_title: Lisboa - Drupal Meetup
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-05-04 14:30:00.000000000 +01:00
    event_start_value2: 2013-05-04 17:30:00.000000000 +01:00
  node_id: 145
layout: evento
title: Lisboa - Drupal Meetup
created: 1366842696
date: 2013-04-24
---
<p>Encontro da comunidade Drupal em Lisboa.</p><p>Mais informações:&nbsp;<a href="http://groups.drupal.org/node/295708#comment-918093">http://groups.drupal.org/node/295708#comment-918093</a></p>
