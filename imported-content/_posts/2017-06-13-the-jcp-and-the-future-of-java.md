---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.meetup.com/pt-jug/events/240482167/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-06-14 18:00:00.000000000 +01:00
    event_start_value2: 2017-06-14 18:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 501
layout: evento
title: The JCP and the Future of Java
created: 1497388380
date: 2017-06-13
---
<p>On the next June, 14th, we are happy to have <a href="https://twitter.com/heathervc">Heather VanCura</a> to talk about the <a href="https://jcp.org">JCP</a> and help us getting involved in the process.</p><p>This is an unmissable event for all of you that are interested in participating in the Adopt-a-JSR program!</p>
