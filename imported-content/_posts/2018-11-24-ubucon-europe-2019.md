---
categories: []
metadata:
  event_location:
  - event_location_value: Sintra, Centro Cultural Olga Cadaval
  event_site:
  - event_site_url: https://sintra2019.ubucon.org/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-10-09 23:00:00.000000000 +01:00
    event_start_value2: 2019-10-12 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAPAD41sMiwM4VLUzPZkNA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.38803201219572e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.9382499456433e1
    mapa_left: !ruby/object:BigDecimal 27:-0.9382499456433e1
    mapa_top: !ruby/object:BigDecimal 27:0.38803201219572e2
    mapa_right: !ruby/object:BigDecimal 27:-0.9382499456433e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.38803201219572e2
    mapa_geohash: eyckdrzv5bvr6tm2
  slide:
  - slide_value: 0
  node_id: 635
layout: evento
title: Ubucon Europe 2019
created: 1543079512
date: 2018-11-24
---
<p>A Ubucon é um evento organizado pelas Comunidades Ubuntu em todo o mundo.</p><p><img src="https://sintra2019.ubucon.org/wp-content/uploads/2019/03/LT_UbuCon_EU_Sintra-RGB.png" alt="Cartaz da Ubucon Europe 2019" style="vertical-align: middle; display: block; margin-left: auto; margin-right: auto;" width="800" height="481"></p><p>O foco do evento é o Ubuntu, uma distribuição de Software Livre guiada pela comunidade, mas também outras tecnologias de Software Livre.</p><p>Este ano, o evento está a ser organizado em Sintra, e ocorrerá em Outubro de 2019. Estamos a preparar quatro dias cheios de apresentações, conferências, workshops, sprints e eventos sociais para todos os participantes.</p><p>A Ubucon Europe 2019 é patrocinada pela ANSOL.</p>
