---
categories:
- security
- libre software
metadata:
  event_location:
  - event_location_value: Polytech Lille, Lille, France
  event_site:
  - event_site_url: https://2019.pass-the-salt.org
    event_site_title: Pass the Salt
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-06-30 23:00:00.000000000 +01:00
    event_start_value2: 2019-07-02 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: !binary |-
      AQEAAAABAACATxcJQHDwGj7ITUlA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.50607673419145e2
    mapa_lon: !ruby/object:BigDecimal 27:0.3136382102966e1
    mapa_left: !ruby/object:BigDecimal 27:0.3136382102966e1
    mapa_top: !ruby/object:BigDecimal 27:0.50607673419145e2
    mapa_right: !ruby/object:BigDecimal 27:0.3136382102966e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.50607673419145e2
    mapa_geohash: u0fpzkgg5404bj2j
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 276
  - tags_tid: 316
  node_id: 658
layout: evento
title: Pass the SALT 2019
created: 1552180511
date: 2019-03-10
---
<p>This conference hosts Security and Libre Talks (SALT), workshops, and networking opportunities for professionals in hacking Free Software, as well as in security. The themes of the conference also include Free Hardware, Open Formats, and research projects. The conference language is English and talks can either be long or short.</p>
