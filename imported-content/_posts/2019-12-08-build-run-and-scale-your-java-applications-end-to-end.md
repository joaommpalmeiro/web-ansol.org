---
categories: []
metadata:
  event_location:
  - event_location_value: Coimbra
  event_site:
  - event_site_url: https://www.meetup.com/Coimbra-JUG/events/266894000/?rv=ea1_v2&_xtd=gatlbWFpbF9jbGlja9oAJDkwYTg1Y2Y3LWJhNzYtNGI2NS05ODJjLTgwODU1MjY0NDMzYg
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-12-13 00:00:00.000000000 +00:00
    event_start_value2: 2019-12-13 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 718
layout: evento
title: Build, run, and scale your Java applications end-to-end
created: 1575822653
date: 2019-12-08
---

