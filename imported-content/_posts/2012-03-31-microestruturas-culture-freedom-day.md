---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: http://www.flausina.com/sonicarea.html#microestruturas
    event_site_title: Microestruturas - Culture Freedom Day
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-05-10 23:00:00.000000000 +01:00
    event_start_value2: 2012-05-11 23:00:00.000000000 +01:00
  node_id: 16
layout: evento
title: Microestruturas - Culture Freedom Day
created: 1333182193
date: 2012-03-31
---
O Culture Freedom Day é uma iniciativa para que se celebre anualmente a Liberdade na Cultura. Celebrado pela primeira vez em 2012, as celebrações em Lisboa serão antecipadas, ocorrendo nos dias 11 e 12 de Maio, inseridas no programa "Microstructures", na Flausina.
