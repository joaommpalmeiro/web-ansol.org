---
categories: []
metadata:
  event_site:
  - event_site_url: https://github.com/marado/obrasorfas/blob/master/obras-orfas.md#calend%C3%A1rio
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-10-29 00:00:00.000000000 +00:00
    event_start_value2: 2014-10-29 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 237
layout: evento
title: Prazo limite para a transposição da directiva sobre Obras Órfãs
created: 1413553345
date: 2014-10-17
---
<p>Em 2012 foi aprovada uma Directiva Europeia em relação a Obras Órfãs, que tem de ser transposta pelos estados membros até 29 de Outubro de 2014.</p><p>A ANSOL prevê que Portugal se prepara para não implementar esta directiva atempadamente.</p>
