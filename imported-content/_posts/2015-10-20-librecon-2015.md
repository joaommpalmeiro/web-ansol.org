---
categories:
- hardware aberto
- software livre
- software aberto
metadata:
  event_location:
  - event_location_value: SANTIAGO DE COMPOSTELA
  event_site:
  - event_site_url: http://www.librecon.io/
    event_site_title: LIBRECON 2015
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-10-29 00:00:00.000000000 +00:00
    event_start_value2: 2015-10-30 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 105
  - tags_tid: 41
  - tags_tid: 106
  node_id: 352
layout: evento
title: LIBRECON 2015
created: 1445378145
date: 2015-10-20
---
<p>O MAIOR ENCONTRO PARA OS AXENTES DO SOFTWARE LIBRE E OPEN TECHNOLOGIES.<br><br>LibreCon 2015 recolle o testigo das 4 edicións anteriores de LibreCon e Libre Software World Conference plantexando un evento cuxo principal obxetivo será a creación de Negocio e Emprego en todos-los sectores da sociedad a través das tecnoloxías libres, a innovación e o emprendemento.<br><br>Con este obxectivo sempre en mente LibreCon 2015 quere demostrar, ademáis, os beneﬁcios dun sector que está sobrevivindo con éxito a situación xeral de crisis. As tecnoloxías abertas en xeral e o Software Libre en particular constitúen unha alternativa real que ofrece a empresas e administracións públicas de todo o mundo unha maior competitividade, grandes aforros e desenrolo de economías locales.<br><br>Este ano, a LibreCon xirará arredor da electrónica de consumo e o sector da automoción (sistemas de entretemento, automóvil conectado), Internet Of Things e a nova educación. Se reservará un espazo para o e-Commerce.<br><br>De forma transversal vertebraránse os itinerarios de accións e ponencias enfocados aos públicos obxectivo do evento, dando unha especial importancia á difusión de casos de éxito no uso das tecnoloxías libres, en cada un destes eixos.</p>
