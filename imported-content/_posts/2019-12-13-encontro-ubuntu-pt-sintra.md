---
categories: []
metadata:
  event_location:
  - event_location_value: Saloon, Sintra
  event_site:
  - event_site_url: https://www.meetup.com/ubuntupt/events/266748573/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-12-19 20:00:00.000000000 +00:00
    event_start_value2: 2019-12-19 20:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 720
layout: evento
title: Encontro Ubuntu-pt @ Sintra
created: 1576245816
date: 2019-12-13
---

