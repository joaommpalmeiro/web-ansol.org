---
categories:
- software freedom day
metadata:
  event_location:
  - event_location_value: Lisboa, ISCTE, B.204
  event_site:
  - event_site_url: http://wiki.softwarefreedomday.org/2014/Portugal/Lisboa/MOSS
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-09-20 10:00:00.000000000 +01:00
    event_start_value2: 2014-09-20 12:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 47
  node_id: 227
layout: evento
title: Software Freedom Day 2014
created: 1411077322
date: 2014-09-18
---
<p>O Mestrado OSS do ISCTE organizou um <a href="http://wiki.softwarefreedomday.org/2014/Portugal/Lisboa/MOSS">evento Software Freedom Day</a> no ISCTE na sala B.204 para este Sábado dia 20 das 11 às 13 com o seguinte programa:</p><ul><li>Apresentação - Prof. Carlos Costa</li><li>Software Freedom Day | ANSOL - Rui Seabra</li><li>Free Software Foundation Europe - MOSS</li><li>Debates e coffee break</li><li>Almoço convívio</li></ul>
