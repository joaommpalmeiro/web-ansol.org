---
categories: []
metadata:
  event_location:
  - event_location_value: IST, Lisboa
  event_site:
  - event_site_url: http://www.meetup.com/pt-jug/events/232970249/?rv=ea1
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-09-05 18:00:00.000000000 +01:00
    event_start_value2: 2016-09-05 18:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 442
layout: evento
title: Putting Hypermedia Back in REST with JAX-RS
created: 1470405365
date: 2016-08-05
---
<div id="event-description-wrap" class="line redactor-description"><p>Para O 27º encontro do PT.JUG tem o prazer de contar com a presença do Sebastian Daschner, que vai demonstrar o que podemos estar a perder ao não usarmos Hypermedia nos nossos endpoints REST.</p></div><div class="line redactor-description">&nbsp;</div><div class="line redactor-description">Abstract: <strong>Putting Hypermedia Back in REST with JAX-RS</strong><p>Everybody is doing REST -- at least everybody claims they are. But mostly web APIs are built without Hypermedia, what would allow the client to follow related resources and autonomously navigate through the API without prior implicit logic of the application.</p><p>This sessions shows the concept and benefits of a Hypermedia driven REST API and how to implement this using JavaEE 7 with JAX-RS.</p><br><p>Bio: <strong>Sebastian Daschner</strong></p><p>Sebastian Daschner is a Java freelancer working as a Consultant / Software Developer / Architect and is enthusiastic about programming and Java (EE). He is participating in the JCP, serving in the JSR 370 Expert Group and hacking on various open source projects on Github. He is a Java Champion and has been working with Java for more than 6 years. Besides Java, Sebastian is also a heavy user of Linux and container technologies like Docker. He evangelizes computer science practices on <a href="https://blog.sebastian-daschner.com">https://blog.sebastian-daschner.com</a> and on Twitter via <a href="https://twitter.com/DaschnerS">@DaschnerS</a>.</p></div>
