---
categories: []
metadata:
  event_site:
  - event_site_url: http://www.maisoeste.pt/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-06-04 20:00:00.000000000 +01:00
    event_start_value2: 2013-06-04 21:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 170
layout: evento
title: DAR em conversa aberta
created: 1370120461
date: 2013-06-01
---
<p>A ANSOL foi entrevistada no programa de rádio "DAR em conversa aberta", na rádio "maisoeste".</p><p>Gravação do programa <a href="https://www.dropbox.com/s/nsaag28sefvqena/radio%20mais%20oeste_2013_06_04.mp3">aqui</a>&nbsp;(60 minutos).</p><p>Excerto do programa com a parte da ANSOL <a href="https://www.dropbox.com/s/tb73nl05qcunmcl/ANSOL_radio%20mais%20oeste_2013_06_04.mp3">aqui</a> (23 minutos).</p>
