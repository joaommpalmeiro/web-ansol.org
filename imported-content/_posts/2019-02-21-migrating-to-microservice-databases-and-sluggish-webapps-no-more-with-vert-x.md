---
categories: []
metadata:
  event_location:
  - event_location_value: Sala VA2, Pavilhão Civil, Instituto Superior Técnico, Lisboa
  event_site:
  - event_site_url: https://www.meetup.com/pt-jug/events/259159517/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-02-27 18:30:00.000000000 +00:00
    event_start_value2: 2019-02-27 18:30:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 649
layout: evento
title: Migrating to Microservice Databases & Sluggish webapps no more with Vert.x
created: 1550768451
date: 2019-02-21
---

