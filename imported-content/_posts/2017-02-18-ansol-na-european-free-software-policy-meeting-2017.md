---
categories:
- ansol
- eu
- policy
- política
- imprensa
metadata:
  tags:
  - tags_tid: 33
  - tags_tid: 94
  - tags_tid: 193
  - tags_tid: 192
  - tags_tid: 19
  node_id: 486
layout: article
title: ANSOL na European Free Software Policy Meeting 2017
created: 1487427377
date: 2017-02-18
---
<p><a href="https://www.flickr.com/photos/136748928@N05/32052184094/in/album-72157680328491395/" title="Pre-FOSDEM 2017" data-flickr-embed="true" data-footer="true"><img src="https://c1.staticflickr.com/1/392/32052184094_d2d19ef475_z.jpg" alt="Pre-FOSDEM 2017" style="display: block; margin-left: auto; margin-right: auto;" width="640" height="427"></a></p><p>A ANSOL esteve presente no encontro Europeu de políticas para o Software Livre, em Bruxelas, em Fevereiro de 2017. Neste encontro organizado pelo segundo ano consecutivo pela <a href="https://fsfe.org/index.pt.html">Free Software Foundation Europe</a>, de quem a ANSOL é associada, e pelo <a href="http://www.openforumeurope.org">OpenForum Europe</a>, houve uma partilha de ideias entre a Sociedade Civil e decisores, com muita troca de informação sobre o que está a ser feito e os caminhos a seguir de futuro.</p><p>Sebastian Raible (assistente parlamentar da Eurodeputada Julia Reda), Pierre Damas (Head of Sector de Digital Services na DIGIT/Comissão Europeia), Jaana Sahk-Labi (da representação da Estónia na EU) e Laurent Joubert (do Governo Francês) apresentaram a sua visão daquilo que está a ser feito a nível Europeu e dos seus Estados-Membros com o uso do Software Livre para a melhoria da sociedade. Um resumo do evento pode ser encontrado, em Inglês, <a href="http://www.openforumeurope.org/european-free-software-policy-meeting-2017/">no site da OpenForum Europe</a>.</p>
