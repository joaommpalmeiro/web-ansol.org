---
categories: []
metadata:
  event_location:
  - event_location_value: Berlim
  event_site:
  - event_site_url: https://www.fsfe.org/summit16
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-09-01 23:00:00.000000000 +01:00
    event_start_value2: 2016-09-03 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 440
layout: evento
title: FSFE summit 2016
created: 1469920688
date: 2016-07-31
---

