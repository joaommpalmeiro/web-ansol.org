---
categories:
- imprensa
- '2002'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 19
  - tags_tid: 21
  node_id: 148
layout: page
title: Presença na Imprensa de 2002
created: 1019994380
date: 2002-04-28
---
<h3 id="head-1eae5bf379d06408eb754e401e2649802815e739">Software Livre</h3><ul><li><p class="line891"><em><a href="https://ansol.org/sites/ansol.org/files/Exame_Informatica-Dez2002.pdf" class="http">À Conquista do Mundo</a></em>, Hugo Séneca, Exame Informática, nº 90, Dezembro de 2002, pág. 84.</p></li><li><p class="line891"><em><a href="https://ansol.org/sites/ansol.org/files/Visao-Maio2002.pdf" class="http">Permitido Copiar</a></em>, Ana Correia Moutinho, Visão, nº 479, 9 a 15 de Maio de 2002, pág. 104.</p></li><li><p class="line891"><em><a href="http://quark.fe.up.pt/ansol/Vida_Economica-Jan2002.pdf" class="http">Portugal já tem mercado para o software livre. Recém-formada ANSOL pretende dinamizar o sector</a></em>. Vida Económica, Nº 936, 25 de Janeiro de 2002, pág. 33.</p></li></ul><p class="line867">&nbsp;</p><h3 id="head-f67476adb704c011ef7c649568a03afae5a1b55c">Porto Cidade Tecnológica 2002</h3><ul><li>Entrevista a Jaime Villate na Rádio Renascença, em ocasião do Porto Cidade Tecnológica 2002, 25 de Novembro de 2002.</li><li><p class="line891"><em><a href="http://quark.fe.up.pt/ansol/Publico-Nov2002.pdf" class="http">Câmara do Porto promove "rave informática" no Pavilhão Rosa Mota</a></em>, Soraia Abdula, Público, nº 4633, 26 de Novembro de 2002, pág. 52.</p></li><li><p class="line891"><em><a href="http://quark.fe.up.pt/ansol/Povoa_Semanario-Nov2002.pdf" class="http">Cidade Tecnológica 2002</a></em>, Bruno Fonte, Póvoa Semanário, nº 209, 27 de Novembro de 2002, pág. 31.</p></li><li><p class="line891"><em><a href="http://quark.fe.up.pt/ansol/JUP-Nov2002.pdf" class="http">Software Livre "Non Stop"</a></em>, Jornal Universitário do Porto, Novembro de 2002, pág. 4.</p></li><li><p class="line891"><a href="http://quark.fe.up.pt/ansol/protocolo_PCT.pdf" class="http">Protocolo</a>&nbsp;entre a ANSOL, o Município do Porto, a Associação Gabinete de Desporto do Porto e a Faculdade de Engenharia da Universidade do Porto.</p></li><li><p class="line891"><a href="http://bin.ansol.org/" class="http">cobertura do evento com fotos e audio</a></p></li></ul><p class="line867">&nbsp;</p><h3 id="head-4b20de912212d9d516d9f315d7fb07b70007a09a">Legislação (EUCD)</h3><ul><li><p class="line891"><em><a href="http://quark.fe.up.pt/ansol/Expresso-Nov2002.pdf" class="http">Software Litigioso</a>. Um conflito entre direitos de autor e liberdade de expressão</em>, L. M. F., Expresso, nº 1570, 30 de Novembro de 2002, Cartaz, pág. 12.</p></li></ul>
