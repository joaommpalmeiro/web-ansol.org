---
categories: []
metadata:
  event_location:
  - event_location_value: Faculdade de Motricidade Humana - Universidade Técnica de
      Lisboa
  event_site:
  - event_site_url: http://www.fmh.utl.pt/semimelisboa/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-02-01 00:00:00.000000000 +00:00
    event_start_value2: 2013-02-02 00:00:00.000000000 +00:00
  node_id: 111
layout: evento
title: VII Seminário - "Exclusão Digital na Sociedade de Informação"
created: 1354579702
date: 2012-12-04
---

