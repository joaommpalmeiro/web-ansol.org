---
categories:
- ilovefs
metadata:
  event_site:
  - event_site_url: http://fsfe.org/campaigns/ilovefs/2015/ilovefs.pt.html
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-02-14 00:00:00.000000000 +00:00
    event_start_value2: 2015-02-14 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 56
  node_id: 272
layout: evento
title: Eu <3 Software Livre 2015
created: 1422372091
date: 2015-01-27
---
<div style="float: left; padding: 1em;"><img src="https://ansol.org/sites/ansol.org/files/ilovefs-heart-px.png" alt="#ilovefs"></div><p>Todos os anos a 14 de Fevereiro, a ANSOL junta-se à Free Software Foundation Europe e pede a todos os utilizadores de Software Livre que pensem naquelas pessoas trabalhadoras na comunidade de Software Livre e mostrem o seu apreço individualmente neste dia de "Eu adoro o Software Livre".</p><p>Tal como no ano passado, a campanha é dedicada às pessoas por detrás do Software Livre porque eles<br>permitem-nos usar, estudar, partilhar e melhorar o software que nos permite trabalhar em liberdade. Desta vez o foco vai especialmente para as pequenas contribuições para o grande conceito da comunidade de Software Livre.</p>
